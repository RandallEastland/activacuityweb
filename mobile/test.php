<?php

// this script receives HTTP requests related to user subscription updates from the ActivAcuity web site.
// it decrypts the request and then hands that request off to the appropriate script (add, renew, cancel).
// For subscriptions that come directly from the mobile app, see 'create_user.php'

$path = $_SERVER['DOCUMENT_ROOT'];

$connection 	  = $path . '/scripts/connection.php';
$newSubscriber 	  = $path . '/mobile/addNewSubscriber.php';
$renewSubscriber  = $path . '/mobile/renewSubscriber.php';
$cancelSubscriber = $path . '/mobile/cancelSubscriber.php';
$notifier 		  = $path . '/mobile/emailUponFailure.php';

include ($connection);
include ($newSubscriber);
include ($renewSubscriber);
include ($cancelSubscriber);
include ($notifier);

date_default_timezone_set('GMT');
$timestamp = date("Y-m-d H:i:s");

$logging = false;

if ($logging) {
	$log = 'subscriptionResultFromWeb.txt';
	$line_separator ="===============================================================================================\n";
	error_log($line_separator, 3, $log);
	error_log("Attempting to update user subscription record.\nTimestamp:" . $timestamp . "\n", 3, $log);
	error_log($line_separator . "\n", 3, $log);
}

// read the key from a file
$key = file_get_contents("key.file");

// $rawString = stripslashes(trim(file_get_contents("php://input"), '"'));
$rawString = '{
"ciphertext": "bGpKNFREcnZZeWZMdzVsY2JtL0hxRHRBSVY0STVEVXlwRUp6VWpCK0ZTUlVSdjZqT2RzMUJabkowbnRTdFRObFhDTzNLTTVkZW1wWUVmcURiTXIyMDFVR25UZjB6N2dsdGFXa1cvT05KdXRJcWdyTllPSlc5NUZ4QXNVRFpaV3dERUJSRzdTWU5lbWdQdkRYa2ZqV0gxbkw1MHJkTmhhT1k2Y0Nic2RjRkFvYkswWDJ3L0hQTndFSG14b0l3WHBiZkpST0ZBSG5laU50ODRyYTBoc0gzNUNMUmdqMWdpbklaMndFb2x2UmdadEZodS9MOXQzMjVLR2RORkI0Mi9JMGdERDhpbWpUc3hJYTJwRWNqVnBlTytIODV0NWFVWS96UVBVempVUWJOUmtYVFd6Y05HVGprUkpRbFFkQjl1dVZvdDdENjZCKzZjZkZ4NnYrRG1yd3l6UkZSYjY5aWcrV1psbHh3cnhyQWRWZjVVaHNRencyb0NvaFM4TVFTR04vcnNXaWEwMWNrZ3pTLzl5MTJ0OW9sKzBKUHlocUs2NTZVZWNGVWUvdHlMdlFBeXZGMlBydkhEckp4SjM5dlVmVDRWS0NMeHBDTmwxelcrZ2pvZWUrb0hmN2piOE52a2pEOTFXQm9MNkFFVjh2eXhiTEF1YVF3L01QK2xpaERGeWsxcGhZUTViZjJSRUVCbmgzQk13emp6SkF3a0J2OG9LOGthQ2wxem1ydGpBOXRjbCtGVUMxUkR0WVIwaVp3ZFk1WmVRNUwyZ2MzMnhMZjUyS29JSlA2N3JPczdrVi9oVThWYW9xc2t5R0RIS0JJQzR1L0pMbnFEWlJVYUoyY09CZlJWYjNqR0R3S0NzeGErZFltejdacWFVTU5lUUVmK2ZFazMxMjNNc0EwWjNoZjFuckl0UXFsWGlkbDd4QUI3ZjZwMWFaRS9DTUEyaE9oZit4UkF3NDg0emc0cjI3MHJ5S05GSGtyRUh4MGRRY096bkpkNVB4cWdjN1Rwa1kxVFI1VEdEOFAwTGVTeXdsaUtrelBLNFBGa2phZDRScjZJRDNYMlJBclhHRmdLMk9oK20rS0R6VE5ENGh0MVpJYlRnZk1GK2QzYTRlVHJHTTk0VldRc3BUTCttSGM0eG1TNzdTY1B2MXduZWtGYlUxblZhNDdwTGdIS0dQaUVZd1hmaWE5ZlBBQTdZa2Exam12MFBuNzF5YnhoUDJaY1B3YzhMTUxhVHJrcWx6MFNHRGZNVUxieEhqNHRDdFhrdlRrWkN5ekwzTmh1SWNQS3lBN3EreGRFRlROTERxbXlrOVkrdGt1SXRVNzhDVUl4OXlidVNPTzNGN1dvVk1ZK2dqZTIvc1lwcFM5UEZ5ZU90QUp1ZUFYQWp1ZnI5RlFhUGZ4NVBsS1gyNDdSOFdlU3ZQRU1yT1o2OVpKVjhVa2xqSzd0RW5CZ1FENWUxd29sdXRDWWJDR3dZOGRBRE00ZVZid1Mrbk9WR2JrTGFDVDZYVzd0YXA1YklrSzN6Wnd5ZWFjbWJrbk5ycGRwRGxOTlRYWHE4TnF3dlVlWk1mQmFTNzgxSCtxdk5oL1lrb1JLdjE1aC9rdTd3K1hVS0dYUEU3TU5wcTdDK3VObWQ0QkJmYWhmQ1FseERzZlhPTVJ2eDc0VHg4S3pjc1lPRERzU0FESHlrajJTWUNPNDVQM0FaQi9zejZLQVRKbEM4RFdMOHNCOXBhOUVNc2Rzd2gvayszRWxyWW4zdmNwNnA3M0VKSkVsTGJQRVdPcUtBS09ZWHVRLzYySm9zTGdsZ2xPbC93LzZYR1JOTkVxbTZINCtnVXpGOGk3UGx3L09UQTY2UHorUkRiT1ZLeFc5MndraTJZSEtaek81cHRQRmV2bk5CSlpDdXpiUTBYNUNXdXdXRHVJcTQ4enVmNFZWMTA1ZzliVmVzTlkxU2xFZG91T1BncW5CekhZN3c1UVRQK1BKNzhnMWJaMFExRzZJdzBRMFVqWCtySXA3ZXM4cGlKNnZnVk1KRVpuZmpqU3FlTUdpRHZVWXloL1J6cDFPR2dGWGZqbjdERllnR3E1SGpwQ1VVRGZRbGZ2cnFLWUhIeWRtMlk4ZXNEZ21wRFJiek02TUhEVHd1SmwzYjhWL1N2WDBBY0w1WVY0ZkdSb3duUWdKZWpySDF0ZGxXMGpqRVltL0FBdlhHQXpmTXdmVG5HdlEvTlNBdUluRVcyM0lndFdWMHV3eXE2UGVkKzVBbUJ6UHN6RHR5TFFJZEZKUWxtMnl5aVBNVzUvc1QxSW1yYm1oWHVLOFFVaUpzSnpNaXNEVWxXZlBkSzdhVXBPUDZPeHdpNjZCOHJNRlQ4TWxhSnpHWGFpNS9jeFltMUxodlA3RnBpMkRWZ2tYbk0yem9RQVBhZGxaR1U5M0lvSEw4RjBQZm94MjJWMUVSd294c3YxVzNsOExkVENldGx1c01Ga0tOdWpKM1B3ZDRHSzQ1Ym0wOHNoMEpzQ25IenhQRmMzeEg1VnJkM2xUR25tTGlILzduM3p3SkJ3OXMvaXRuTG4xTkNQZ0VReEc5OEdEQlN6R2QvTnBaejNFZUVwMjMwaVdzMXVianhlay9KUWt0c0hvRlpOamxzVTRmYkkyWElzdllXZDE1L2c5ZFZJNDNRNWNxSXdhYVR2SDZjNkh6c3diU21DZXFEU0dKNUhXSXFJTHc2MitHRzRybnB0WmJ3TmZlclJydFM3c1ZDaEhEWWRkc2U5WXZKdXhybEZMSkFEN2F6YVFtcGFheWpGMWFjdXdNYUlNcjZPdGNSMU1WYVRRb1NJNm1RbjlXTmNUQXhCUi82N3VQL240VGJwaGNzeHllSEVlVnZPTWZla0J4NnJrZkVsVzBQN1EzbjU1N056TnBZMzdGZDI1bW1GUHp0KzFhdmVuTE5vc0IyNGhWMm9SYmlRWUV3aWNNbWZDREY1RER3cW10M3RjN28xV29FUnRUVFRUVFlsWDRNcFM0MWtPWTJmNE96UmRnV3drVHhRTXh0cVQ2eUFxdms2LzdwcGdEMkx2cnpFbU5ZRDlJODdUNDF5R2NCbVprcVc0VWYveGRsc0Nrd3NCNjB6dXk5dkV4ZitBRXdwaCtLQzdFaGs5c2NXYkd2bVdkQzR4Um5FOENlMkkzdUVib3VnZXNjaWg4OU1nc2N0WWJySFJleXcyQmcrQlVEN0FQeTV1RVRlWHlSK0ZPWjRKWThhSXRhbWlRLzlLZlV5dEhFYUI0alRUZ1V3Q1FNWHZPWjVVTVpBUU9jSVNWSXBCd1c5bEZVY3N2dG42UEVOSDl2NkdyMEgzditsVld6YjQwQVZQR0NlMGhUNnlFTU9GYXBTZFkxVEJxdjN4dlpvZnY1NTREMDVEbERPWDUwZTZBOHVlQzdPUFJFelRNTU5hNGNjcXhBVW1CTzBPTldka3lWSlRCeFdLTzl2SEdxckw2cUluYzZRRnQxc285a3c5TlZGRjNVbU5WckRTUnM0T0RndVM3dlFpcTFnd1MyU1hPWEx6S2kydXAvcC92bmVhMExyUTl2WDU0UHZUdEZGTitQQWFPZWJHOHNMdVVSRjdJUVEwRDhGdTh6NzBZUEZjSU9MKzhuNFVvVWk4eG1YYWhUc0lBMThtSDFxa1B3V2p3a1o3QXpoNVFNNnpYUTVzazdGZi9MaFBPWG1adE9ULzJ0ZHBETlVkakdvdWw3MEw2blhEWUZ5c1lTKzBDc0NVK2FhQUFFVk5ad1VIdXpUb2VOVUhOYUF4clIyTVlNRDQ4TjhReXFZaG1UdFVXaXJKb2dMb3ZhQWVJdy80K1IzOGpqemFDUm4zZFdtb3UvNHlzOGdaZlNxdzg5MjNWc1hOK2xhMVplL0pWVFk4N1N5V1RpeEg3S0k1c0pKTFVqTmFucGZCZHNpWVNGUGdFamxST2dqMEI1b0NlTjZsVzU0N0FERlFwSC8yYjZrV1JTbDdpK1BrSVlaSFFRL2RTNjYrb3ZvSHlJenRobHE4c1NEVm91clJycDBoSWVYYW9ZVUVYOHVZSktZNVQvNHY4SlFvdDR1aS9ScjA1dTJoUjRscnQrblJra2ZNSHF0ZmJlaHd0TWNSVVlEVjRFVlRIZGN4WnZnN0J0OHFPbG1IYjlSME1iLzZnbWV4dUN6aWZ2VE1nS3JRclIrV29WTUpQTHBYNWJlUkdHK0R4NHJnNDc5SjZNQ1dLMHFFYkQwTzZ5NVF6cmlSbnhNMDEwS0RjZ3BqVlNRVm9VamNuZlA4TEJ6WE4zVktkaGVSbll5VExuT3g0d2h1SGxuNkxteHNHUnNPVXpteU1RYnNraVd1dGlhTit0Sng5emhERWZJVWdSclE5clV0UUtRWm9qSXRJN2NNTE43TnBjeGNzajRjRmVaVGZ6Z1c2ZWgrR204ZlUvSHlTRzB3Um5rMHlncmFTbUNZMjJmbllRczd6bGtxeWp4Z25uRkR6K3NQa1RMOXk2Tk1HOWJmVmdPeVZFVlYxaUphbGdRVUp3WCtiWHI1bkVUdkZPMU5RMlozaXUxMWlOSWp5bGF0M2tSbVlWQTV6ejVRRTVRVU9LSldMK2hMcjh6TXZUMnROL0xpcVNmalFiT0FoVEhGT1FvcTZNbFVnUVlsWXNwY2U1aEhUMjdWMWZPdHlVaWdZY3h6R2c5cmU3U0ZCKzVpSjVKZUphUm5XOWg0ZFNKZWhXSmhNL2pKVmpTR29KZVBIc2taa2lmNFI3Q0JsalB6NjV0blFCcjdPaXdTNnFPMWM2Rk0xeUdKL0VMOWZqbS95cmNYeHR1TFJSMWdKQzFXL0JZZ3l3NFVMaFlwMzBJY29iMDc5bm9GZjZheHlxZ3pDM3NYcnhrbEN0TVB2MmRRK0VBVGdUSnk5ZEIvRm5kdnRudW5iUDgyZGc3aGJQSHFIeDYxUm5JTE9EZCtWelBzOW4rSGJVY04wNmxQVzdXSmp0Mmp6NTVCam5McU1HV080VGdqMVdudmZrZ1F2bEF6RkhKajI4N0FxS0REMVViWVpHeWlhZkN6Q2dBcnE2SGNGWEowMkhOUHY5UVk2OFNwaXdxcmNMazZHTURkOFJpS3FaRkNFajg3ckN0TzVHSU80aTdqWDRqWld3VGcrci9JanBZd1Axb094SjFFRm5CRUczTmxaVmI5SzF5TWFMb3V2cHN1bmU3bDZyQSt5dVpDbStBK2JTNllBK3RqbFVMb25QaWdQajlFcnB0VEFFV3cxbSsxcUFWbkNHRTR5c0dZU2ZNTFBFOEZpTVpLaXFSWmFlVEQ1V3ByMzNXOHlLbytqeENISjZ1WjhZVmhnY3B0empPUzdWN1VIeUhDVW5Ga0FEU0QzYno3UUtIQWdGYjB6K2xyTjVBaUdKWEx5U1VwUEdqdTdNZDBocFdXc1JYeHVzNnNhOU9hL01TVkVad3hsYjduMXljWk5vcDlkRGdXNjhDK2Uvd0tDb0ExeWdwbzd0eFE1NGF5YUtuWWtUNEN2ZnhWY1I4Tm1wMzNnZTVLSUtIOE5aQ0ZkVXpzRFY5a1RkSHRVUlZjUjI5TEJ3ZlBKcjkva3hBQzZUdHJldmpFMnc5eUtyVng4Rzgzc2JHNTJEdEpaRGJ2VVA3L3VlWEt0NmdzTm0rOGZsOHk1NWoyREYxdmI2SFRQVklqdkZOcWg3b1pLdThDdVBrSmhnZWFSeUlIcE1ncVlCV25FSnJKTlMvSitKUTNWQ21hMzFGZ2VvdjVMZU5UNjFwbHZ6Snp3aUlKeTk1NStxUkI3a2JVcGVaa3ZoeTk0U1ZiK3hJejkwMmhaK0Z1Vk9nRmxHaFBZckJoRjhyOWxKbzM1UWZRZ0ExK05RZmZzbCt1RVRRZ2JSb29MYU1saDJ0M3hUbkF4LzVJV0lQUThxek1KSEVKZEdkUEVkSHAvajlLaXJyVXgzdE41V0xFb0VXc0lIckJZRG5hUVZiQXZWRWVhVnVwdnFRK0dkNWZ4cG5jdjhrUzg3OC9mR052ZUEzNnl0aU1hcDF4emV4TVk1bmVORWRPN2RJVk1ibFlVTUJOcC9MV0Z1WTFSOVJrdWZsNHlzUU00ZHJyYTJEN1FQeVI1aFB1bHFUQUcwc04wUkR4WHJBT2oxSjQwPQ=="
}';

if ($logging) {
	error_log("RAW TEXT RECEIVED:\n", 3, $log);
	error_log($rawString, 3, $log);
	error_log("\n\n", 3, $log);
	
	// save the encrypted data string to a file for later testing
	$fp = fopen('ciphertext.txt', 'w');
	fwrite($fp, $rawString);
	fclose($fp);
}

// get the ciphertext from the POST parameter
$jObj = json_decode($rawString);
$ciphertext = $jObj->ciphertext;

if ($logging) {
	error_log("CIPHERTEXT RECEIVED:\n", 3, $log);
	error_log($ciphertext . "\n\n", 3, $log);
}

// need to remove the quotation marks that wrap the ciphertext
$ciphertext = str_replace("%22", "", $ciphertext);

for ($i=1; $i<=2; $i++) {
	$decrypted = unserialize(openssl_decrypt(base64_decode($ciphertext), 'aes-128-cbc', $key));
	if ($logging) error_log("Decryption attempt #" . $i . " was ", 3, $log);
	
	if ($decrypted) {
		if ($logging) {
			error_log("successful.\n\n", 3, $log);
			error_log("DECRYPTED RESULT\n", 3, $log);
			error_log(var_export($decrypted, true), 3, $log);
		}
		break;
	
	} else {
	
		if ($i<2) {
			if ($logging) error_log("NOT successful. Trying again now...\n", 3, $log);
		} else {
			if ($logging) {
				error_log("NOT successful. No further attempts will be made\n", 3, $log);
				error_log("DECRYPTION FAILED - ABORTING SCRIPT PREMATURELY\n", 3, $log);
				error_log($line_separator . $line_separator . "\n\n", 3, $log);
			}

			sendEmailUponFailure();
			die();
		}
	}
}

if ($logging) error_log("\n\n", 3, $log);

// for data from Erin
$user_name = htmlspecialchars(strip_tags($decrypted['subscription']['customer']['email']), ENT_QUOTES);
$status    = strip_tags($decrypted['subscription']['status']);
$race	   = strip_tags($decrypted['subscription']['race']);

// strip whitespace
$user_name = preg_replace('/\s+/', '', $user_name);

// verify username availability
//verify that the username does not already exist in the permanent table
$sql = "SELECT user_id FROM users WHERE (user_name='$user_name')";
$result = mysqli_query($mysqli, $sql);

if (!$result) {
	if ($logging) error_log("error determining user name availability\naborting\n", 3, $log);
    echo json_encode(array('result'=>'failure', 'message'=>'error determining user name availability: ' . mysqli_error($result)));
    mysqli_close($mysqli);
    die();
}

if ($logging) error_log("BEGINNING DATABASE UPDATE ATTEMPT\n", 3, $log);
// if (mysqli_num_rows($result) > 0) { // indicates a username that exists in the database
//
// 	if ($status == 'active') { // three possible values: active, cancelled, suspended
// 		// active status means that this is a renewal
// 		$updated = renewSubscriber($mysqli, $decrypted, $log, $logging);
// 	} else { // cancelled or suspended
// 		$updated = cancelSubscriber($mysqli, $decrypted, $log, $logging);
// 	}
//
// } else { // username does not exist in the database -> add it
	$updated = addNewSubscriber($mysqli, $decrypted, $log, $logging);
// }

die();

if ($logging) error_log("DONE WITH DATABASE UPDATE ATTEMPT\n\n", 3, $log);

mysqli_close($mysqli);

if ($updated) {
	if ($logging) error_log("Script completed SUCCESSFULLY\n\n", 3, $log);
	echo json_encode(array('result'=>'success', 'message'=>'successfully updated user record'));
} else { 
	if ($logging) error_log("Script FAILED to complete successfully\n\n", 3, $log);
	echo json_encode(array('result'=>'failure', 'message'=>'an error occured'));
}

if ($logging) {
	error_log("END OF UPDATE ATTEMPT\n", 3, $log);
	error_log($line_separator . $line_separator . "\n\n", 3, $log);
}
?>