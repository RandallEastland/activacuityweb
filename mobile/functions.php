<?php

/**

* Class and Function List:

* Function list:

* - init()

* - constants()

* - widgets()

* - supports()

* - functions()

* - post_type()

* - admin_menus()

* - _load_demo_content_page()

* - language()

* - _load_option_page()

* - admin()

* Classes list:

* - Theme

*/

$theme = new Theme();

$theme->init(array(

	"theme_name" => "Lucidpress",

	"theme_slug" => "LP",

	"enable_wpml" => false

	

	//enabling WPML feature will cause all masterkey settings reset to default, so please make sure you will change this option to "true" before manipulating theme options

	

));



class Theme

{

	function init($options)

	{

		$this->constants($options);

		add_action('init', array(&$this,

			'language'

		));

		$this->functions();

		$this->post_type();

		$this->admin();

		add_action('admin_menu', array(&$this,

			'admin_menus'

		));

		add_action('after_setup_theme', array(&$this,

			'supports'

		));

		add_action('widgets_init', array(&$this,

			'widgets'

		));

	}

	

	function constants($options)

	{

		define("THEME_DIR", get_template_directory());

		define("THEME_DIR_URL", get_template_directory_uri());

		define("THEME_NAME", $options["theme_name"]);

		define("THEME_SLUG", $options["theme_slug"]);

		define("THEME_STYLES", THEME_DIR_URL . "/styles/css");

		

		//enables WPML feature

		if (defined("ICL_LANGUAGE_CODE") && $options["enable_wpml"] == true) {

			$lang = "_" . ICL_LANGUAGE_CODE;

		} 

		else {

			$lang = "";

		}

		define("THEME_OPTIONS", $options["theme_name"] . '_options' . $lang);

		

		define("THEME_CORE", THEME_DIR . "/core");

		define("THEME_IMAGES", THEME_DIR_URL . "/images");

		define("THEME_ASSETS", THEME_DIR_URL . "/assets");

		define("THEME_JS", THEME_DIR_URL . "/js");

		define('THEME_FONT_URI', THEME_DIR_URL . '/cufon');

		define('THEME_FONT_DIR', THEME_DIR . '/cufon');

		define('FONTFACE_DIR', THEME_DIR . '/fontface');

		define('FONTFACE_URI', THEME_DIR_URL . '/fontface');

		

		define('THEME_ADMIN', THEME_CORE . '/admin');

		define('THEME_METABOXES', THEME_ADMIN . '/metaboxes');

		define('THEME_ADMIN_POST_TYPES', THEME_ADMIN . '/post-types');

		define('THEME_GENERATORS', THEME_ADMIN . '/generators');

		define('THEME_ADMIN_URI', THEME_DIR_URL . '/core/admin');

		define('THEME_ADMIN_ASSETS_URI', THEME_DIR_URL . '/core/admin/assets');

	}

	

	function widgets()

	{

		require_once THEME_CORE . "/widgets.php";

		register_widget("Artbees_Widget_Popular_Posts");

		

		register_widget("Artbees_Widget_Recent_Posts");

		register_widget("Artbees_Widget_Related_Posts");

		register_widget("Artbees_Widget_Twitter");

		register_widget("Artbees_Widget_Advertisement");

		register_widget("Artbees_Widget_Most_popular_tags");

		register_widget("Artbees_Widget_Contact_Form");

		register_widget("Artbees_Widget_Contact_Info");

		register_widget("Artbees_Widget_Flickr");

		register_widget("Artbees_Widget_Social");

		register_widget("Artbees_Widget_Sub_Navigation");

		register_widget("Artbees_Widget_Google_Map");

		register_widget("Artbees_Widget_Testimonials");

		register_widget("Artbees_Widget_video");

	}

	

	function supports()

	{

		if (function_exists('add_theme_support')) {

			add_theme_support('post-thumbnails');

			add_theme_support('menus');

			add_theme_support('automatic-feed-links');

			add_theme_support('editor-style');

			add_theme_support('woocommerce');

			register_nav_menus(array(

				'primary-menu' => THEME_NAME . ' Main Navigation',

				'footer-menu' => THEME_NAME . ' Footer Menu'

			));

		}

	}

	

	function functions()

	{

		include_once (ABSPATH . 'wp-admin/includes/plugin.php');

		require_once THEME_CORE . "/bfi_cropping.php";

		require_once THEME_CORE . "/general.php";

		require_once THEME_CORE . "/dynamic-styles.php";

		require_once THEME_CORE . "/shortcodes.php";

		require_once THEME_CORE . "/theme_class.php";

		include_once (THEME_ADMIN_POST_TYPES . '/portfolio.php');

		require_once THEME_CORE . "/frontend_scripts.php";

		require_once THEME_GENERATORS . '/sidebar-generator.php';

		require_once THEME_CORE . "/mk-woocommerce.php";

		require_once (THEME_CORE . "/tgm-plugin-activation/request-plugins.php");

		require_once (THEME_CORE . "/vc-integration.php");

	}

	

	function post_type()

	{

		include_once (THEME_ADMIN_POST_TYPES . '/portfolio.php');

		include_once (THEME_ADMIN_POST_TYPES . '/slideshow.php');

		include_once (THEME_ADMIN_POST_TYPES . '/clientbox.php');

		include_once (THEME_ADMIN_POST_TYPES . '/pricing.php');

		include_once (THEME_ADMIN_POST_TYPES . '/testimonials.php');

		include_once (THEME_ADMIN_POST_TYPES . '/employee.php');

		include_once (THEME_ADMIN_POST_TYPES . '/edge_slider.php');

	}

	

	function admin_menus()

	{

		add_menu_page(__('Theme Options', 'mk_framework') , __('Theme Options', 'mk_framework') , 'edit_theme_options', 'masterkey', array(&$this,

			'_load_option_page'

		) , 'dashicons-admin-network');

		add_submenu_page('themes.php', 'Install Templates', 'Install Templates', 'manage_options', 'demo-importer', array(&$this,

			'_load_demo_content_page'

		));

	}

	

	function _load_demo_content_page()

	{

		include_once (THEME_DIR . '/demo-importer/engine/index.php');

	}

	

	function language()

	{

		$locale = get_locale();

		if (is_admin()) {

			load_theme_textdomain('theme_backend', THEME_DIR . '/lang');

			$locale_file = THEME_ADMIN . "/lang/$locale.php";

		} 

		else {

			load_theme_textdomain('theme_frontend', THEME_DIR . '/lang');

			$locale_file = THEME_DIR . "/lang/$locale.php";

		}

		

		if (is_readable($locale_file)) {

			require_once $locale_file;

		}

	}

	

	function _load_option_page()

	{

		

		$page = include (THEME_ADMIN . '/admin-panel/masterkey.php');

		new optionGenerator($page['name'], $page['options']);

	}

	

	function admin()

	{

		if (is_admin()) {

			require_once THEME_ADMIN . '/admin.php';

			$admin = new Theme_admin();

			$admin->init();

		}

	}

}

// --- Autocomplete WooCommerce orders  --- //
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

// --- start mod for webhook REST payload  --- //
add_filter('woocommerce_webhook_payload', 'xo_woocommerce_webhook_payload', 97, 4);
function xo_woocommerce_webhook_payload($payload, $resource, $c , $hook_id) {
  
	//$h       = new WC_Webhook($hook_id);
  $user_id = $payload['subscription']['customer_id'];
	$val		 = get_transient( 'xo_'.$user_id.'_xo');
	
  if( $resource == 'subscription' &&  $val )
  {
    $payload['subscription']['account_password'] = $val;
    //$payload = base64_encode(@openssl_encrypt( serialize($payload) , "aes-128-cbc" , "pFJr3YDYSL8jyAcm" ));
		delete_transient('xo_'.$user_id.'_xo');
  }
	
  return $payload;
}

add_action('woocommerce_checkout_order_processed', 'xo_woocommerce_checkout_order_processed', 97, 2);
function xo_woocommerce_checkout_order_processed($id , $posted) {
  if($posted['account_password'])
	{
	  $user_id = get_current_user_id();
		$settt	 = $posted['account_password'];
	  set_transient( 'xo_'.$user_id.'_xo', $settt , 120 );

	}
}

add_filter('woocommerce_webhook_payload', 'encrypt_woocommerce_webhook_payload', 99, 4);
function encrypt_woocommerce_webhook_payload($payload, $resource, $c , $hook_id) {
    
    $payload = base64_encode(@openssl_encrypt( serialize($payload) , "aes-128-cbc" , "pFJr3YDYSL8jyAcm" ));

    // randy- wrap the encrypted text inside a json object
    $payload = json_encode(array('ciphertext'=>$payload));

    return $payload; 
}
// --- end mod --- //