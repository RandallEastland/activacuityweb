<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);
	include 'race_expiration_status.php';
    
    $user_name   = strtolower(strip_tags($_POST['user_name']));
    $password    = strip_tags($_POST['password']);
    
    $sql = "SELECT race_id, hashed_password, expiration_date, bool_must_reset_pw, member_type, subscription_source
            FROM users 
            WHERE (LOWER(user_name)=?)
            LIMIT 1";
    
    if ($stmt = mysqli_prepare($mysqli, $sql)) {

        // Bind the input parameters to the query
        mysqli_stmt_bind_param($stmt, 's', $user_name);

        // execute query 
        mysqli_stmt_execute($stmt);

        /* store result (only necessary for the mysqli_stmt_num_rows command) */
        mysqli_stmt_store_result($stmt);

        // make sure that the result set is not empty
        // (note that if we've retrieve the salt, this cannot be empty)
        if (mysqli_stmt_num_rows($stmt) == 0) {
            mysqli_stmt_close($stmt);
            mysqli_close($mysqli);
			echo json_encode(array('result'=>'failed', 'message'=>'no matching athlete'));
            die();
        }
    
        // bind result variables
        mysqli_stmt_bind_result(
            $stmt, $race_id, $db_password, $expiration_date, $bool_must_reset_pw, $member_type, $subscription_source);

        // fetch values
        mysqli_stmt_fetch($stmt);
        
        //hash the password
        if (crypt($password, $db_password) != $db_password) {
            mysqli_close($mysqli);
			echo json_encode(array('result'=>'failed', 'message'=>'invalid password'));
            die();
        }

		// calculate the number of days left in the user's subscription
		date_default_timezone_set('GMT');
		$today = new DateTime("today"); // pad by 1 day to avoid 'partial day errors'
		$expiry_date = new DateTime($expiration_date);
		$interval = $today->diff($expiry_date);
		
		if ($expiry_date < $today) {
            mysqli_close($mysqli);
			echo json_encode(array('result'=>'expired', 'message'=>'subscription expired', 'subscription_source'=>$subscription_source));
            die();
        }
        
        if ($bool_must_reset_pw == '1') {
            mysqli_close($mysqli);
			echo json_encode(array('result'=>'reset', 'message'=>'password reset required'));
            die();
        }

		// for users affiliated with a race, we need to see if the race is complete
		$race_expiration_status = is_race_complete($mysqli, $race_id);
		
		if ($race_expiration_status['isComplete']) {
            mysqli_close($mysqli);
			$expiration_text = $race_expiration_status['message'];
			echo json_encode(array('result'=>'race_complete', 'message'=>$expiration_text));
            die();
		}

        //everything worked, set up the complete json object
		$array = array(	'result'						=> 'success', 
						'member_type'					=> $member_type, 
						'subscription_days_remaining'	=> $interval->days,
						'subscription_source'			=> $subscription_source
					   );
					   
		echo json_encode($array);

        // close statement
        mysqli_stmt_close($stmt);
    
    }
    
    mysqli_close($mysqli);

?>
