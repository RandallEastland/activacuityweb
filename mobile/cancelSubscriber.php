<?php

// note that this gets called by 'newSubscriberFromWeb.php'. It is for communicating subscription changes that are
// made via the the ActivAcuity web site (not directly within the app)

function cancelSubscriber($mysqli, $data, $log, $logging) {
		
	if ($logging) error_log("This is an attempt to CANCEL an EXISTING subscriber\n", 3, $log);
	
	date_default_timezone_set('GMT');
	
	$user_name  		= htmlspecialchars(strip_tags($data['subscription']['customer']['email']), ENT_QUOTES);
	$updated_at  		= strip_tags($data['subscription']['updated_at']);
	$expiration_date    = date("Y-m-d H:i:s");
	
	$updated_at = date('Y-m-d', strtotime($updated_at));
	
	// strip whitespace
	$user_name = preg_replace('/\s+/', '', $user_name);

	if ($logging) error_log("with user name: " . $user_name . "\n", 3, $log);

	$sql = "UPDATE users
			SET expiration_date=?, updated_at=?
			WHERE user_name=?";
			
	$stmt =  mysqli_stmt_init($mysqli);

	if (mysqli_stmt_prepare($stmt, $sql)) {

	    /* Bind the input parameters to the query */
	    mysqli_stmt_bind_param($stmt, 'sss', $expiration_date, $updated_at, $user_name);

	    /* execute query */
	    if (!mysqli_stmt_execute($stmt)) {
			if ($logging) error_log("SQL update FAILED\n", 3, $log);
	        mysqli_stmt_close($stmt);
			return false;
	    }

	    /* close statement */
	    mysqli_stmt_close($stmt);
	}
	
	if ($logging) error_log("SQL update successful\n", 3, $log);
	return true;
}
?>