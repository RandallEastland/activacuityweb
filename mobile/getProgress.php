<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //get information on this location from the database
    $sql = "SELECT session_id FROM progress ORDER BY session_id";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve progress list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {
        array_push($array, row[('session_id')]);
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
?>
