<?php

function create_transaction($customerToken, $planId) {

    $subscriptionData = array(
        'paymentMethodToken' => $customerToken,
        'planId' => $planId
    );

    $result = braintree_Subscription::create($subscriptionData);
 
    if ($result->success) {
        //$subscriptionId = $result->subscription->id;
        //$transactionId = $result->subscription->transactions[0]->id;
        
        //echo("Subscription Id: " . $subscriptionId . "<br />");
        //echo("Transaction Id: " . $transactionId . "<br />"); 
        
        //print "<pre>";
        //print_r($result);
        //print "</pre>";
        $jsonResult = json_encode($result);
        return json_encode(array(
            'result'            => 'success', 
            'transaction'       => $jsonResult, 
            'subscription_id'   => $result->subscription->id));
        
    } else {
        foreach ($result->errors->deepAll() as $error) {
            $errorFound .= $error->message . "<br />";
        }

        return json_encode(array('result'=>'error', 'errorFound'=>$errorFound));
    }
}
?>
