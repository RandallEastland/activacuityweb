<?php

function getRaceId($mysqli, $woo_commerce_race_id) {
	
    $sql = "SELECT race_id FROM races WHERE woo_commerce_id='$woo_commerce_race_id' LIMIT 1";
    $result = mysqli_query($mysqli, $sql);

    if(!$result) {return 0;}

    $row = mysqli_fetch_array($result);
	if (!$row) {return 0;}

    $race_id = $row['race_id'];	
	if (!$race_id) {return 0;}
	
	return $race_id;
}

?>