<?php

require_once('../braintree-php-2.33.0/lib/Braintree.php');
require_once '../braintree-php-2.33.0/_environment.php';

// this is the version that we send back to the app
$encodedClientToken = Braintree_ClientToken::generate();
//echo $encodedClientToken;

// to display it, all we need to do is base64 decode
// and it becomes a json object
//$clientToken = base64_decode($encodedClientToken);

//print "<pre>";
//print_r(json_decode($clientToken));
//print "</pre>";

if ($encodedClientToken) {
    // send the encode version back to the app
    echo json_encode(array("result" => "success", "token" => $encodedClientToken));
} else {
    echo json_encode(array("result" => "failure", "token" => "none"));
}

?>
