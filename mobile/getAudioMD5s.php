<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);
    $session_verifier = $path . '/scripts/administration/session_verifier.php';


    //get information on this location from the database
    $sql = "SELECT media_file FROM sessions";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve media file list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {

        $md5 = 'x';
        
        $file = $path . '/audio/' . $row['media_file'];
        
        if (file_exists($file)) { 
        
            $md5_array = array(
                'media_file'    => $row['media_file'],
                'md5'           => md5_file($file)
            );

            array_push($array, $md5_array);
        }
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
?>
