<?php

/* This script is called by the mobile app to create a new user record */

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$user_name      = strip_tags($_POST['user_name']);
$first_name     = strip_tags($_POST['first_name']);
$last_name      = strip_tags($_POST['last_name']);
$email          = strip_tags($_POST['email']);
$password       = strip_tags($_POST['password']);
$device_salt    = strip_tags($_POST['device_salt']);
$member_type    = strip_tags($_POST['member_type']);

//strip whitespace
$user_name = preg_replace('/\s+/', '', $user_name);
$email = preg_replace('/\s+/', '', $email);
$password = preg_replace('/\s+/', '', $password);
$member_type = preg_replace('/\s+/', '', $member_type);

//verify that the username does not already exist in the permanent table
$sql = "SELECT user_id FROM users WHERE (user_name='$user_name')";
$result = mysqli_query($mysqli, $sql);

if (!$result) {
    echo json_encode(array("success"=>"failure", "message"=>mysqli_error($result)));
    mysqli_close($mysqli);
    die();
}

if (mysqli_num_rows($result) > 0) {
	echo json_encode(array("success"=>"failure", "message"=>"This user name has already been taken."));
    mysqli_close($mysqli);
    die();
}

//hash the password
// These only work for CRYPT_SHA512, but it should give you an idea of how crypt() works.
$salt = uniqid(); // Could use the second parameter to give it more entropy.
$algo = '6'; // This is CRYPT_SHA512 as shown on http://php.net/crypt
$rounds = '5000'; // The more, the more secure it is!
$cryptSalt = '$' . $algo . '$rounds=' . $rounds . '$' . $salt; // This is the "salt" string we give to crypt().
$hashedPassword = crypt($password, $cryptSalt);

$confirmation_code = md5(uniqid(rand()));
$created_at = date('Y-m-d H:i:s');

//////////////////////////////////////////////////////
//for testing only!!!!!!!!!!!!!!!!
$expiration_date = date('Y-m-d', strtotime($created_at . ' + 365 days'));
//////////////////////////////////////////////////////

// if this user chooses to become a paid subscriber, it will be through the Apple payment service
$subscription_source = 'Not Subscribed';

//everything looks good, attempt to insert new record
$sql = "INSERT INTO users
    	(user_name, first_name, last_name, email, hashed_password, salt, device_salt, 
			created_at, last_renewal_date, expiration_date, updated_at, member_type, subscription_source) 
    	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

$stmt =  mysqli_stmt_init($mysqli);

if (mysqli_stmt_prepare($stmt, $sql)) {

    /* Bind the input parameters to the query */
    mysqli_stmt_bind_param($stmt, 'sssssssssssss', 
        $user_name, $first_name, $last_name, $email, $hashedPassword, $cryptSalt, $device_salt, 
		$created_at, $created_at, $expiration_date, $created_at, $member_type, $subscription_source); 
    
    /* execute query */
    mysqli_stmt_execute($stmt);

    if (mysqli_stmt_affected_rows($stmt) != 1) {
		echo json_encode(array("success"=>"failure", "message"=>mysqli_error($result)));
        mysqli_stmt_close($stmt);
        die();
    }
    
    /* close statement */
    mysqli_stmt_close($stmt);

}

mysqli_close($mysqli);

echo json_encode(array("success"=>"success", "message"=>"Congratulations! You have been successfully registered."));
?>
