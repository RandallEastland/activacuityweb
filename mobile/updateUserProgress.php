<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    $user_name  = $_POST['user_name'];
    $session_id = $_POST['session_id'];

    // first check to see if this user_name, session_id combination
    // already exists in the progres table. If it does, then this user
    // has previously completed listening to this session. If so, no
    // need to do anything. Just exit gracefully.
    
    $sql = "SELECT * 
            FROM progress 
            WHERE user_name=? 
                AND session_id=?";
    
    if ($stmt = mysqli_prepare($mysqli, $sql)) { 

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'si', $user_name, $session_id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* store result (only necessary for the mysqli_stmt_num_rows command) */
        mysqli_stmt_store_result($stmt);

        // check to see if anything came back. If so, exit
        if (mysqli_stmt_num_rows($stmt) > 0) {
            mysqli_stmt_close($stmt);
            mysqli_close($mysqli);
            echo "User has previously listened to this session";
            die();
        }
    }

    // if we get here, there is no record indicating that this user has
    // already listened to the session. Add it.
    
    $sql = "INSERT INTO progress (user_name, session_id) 
            VALUES (?,?)";

    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'si', $user_name, $session_id); 
        
        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            echo mysqli_error($mysqli);
            mysqli_stmt_close($stmt);
            die();
        }

        /* close statement */
        mysqli_stmt_close($stmt);
    }

    mysqli_close($mysqli);

    echo "User Progress Successfully Updated on Server";
    // not sending any reply on this script 
?>
