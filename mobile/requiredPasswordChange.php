<?php

//note: this function is used after the user has logged into the system on a temporary password.
//this temp password has already been verified, so we don't need to do that again. This
//function just changes the password and resets the required pw change bit to 0.

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    $user_name          = $_POST['user_name'];
    $new_password       = $_POST['new_password_hash'];
    $device_salt        = $_POST['new_salt'];

    //Hash the password.
    $salt = uniqid(); // Could use the second parameter to give it more entropy.
    $algo = '6'; // This is CRYPT_SHA512 as shown on http://php.net/crypt
    $rounds = '5000'; // The more, the more secure it is!
    $cryptSalt = '$' . $algo . '$rounds=' . $rounds . '$' . $salt; // This is the "salt" string we give to crypt().
    $hashedPassword = crypt($new_password, $cryptSalt);

    $updated_at = date('Y-m-d H:i:s');

    $sql = "UPDATE users
            SET hashed_password='$hashedPassword', salt='$cryptSalt', 
                device_salt='$device_salt', 
                updated_at='$updated_at', bool_must_reset_pw='0'  
            WHERE user_name='$user_name' ";
    $result = mysqli_query($mysqli, $sql);

    if (!$result) {
        echo json_encode(array('result'=>'failure', 'message'=>'failed to update user record'));
        mysqli_close($mysqli);
        die();
    }

    mysqli_close($mysqli);

    $message = 'successfully updated user record for ' . $user_name;
    echo json_encode(array('result'=>"success", 'message'=>$message));
?>
