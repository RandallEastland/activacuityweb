<?php

require_once '../braintree-php-2.33.0/_environment.php';
require_once '../braintree-php-2.33.0/lib/Braintree.php';

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$user_name = $_POST['user_name'];
 

// get the subscription id that is stored in the database with the original
// transaction array that was received from Braintree when originally subscribing

$sql = "SELECT subscription_id
        FROM users
        WHERE user_name=?
        LIMIT 1";
$stmt = mysqli_stmt_init($mysqli);

if (mysqli_stmt_prepare($stmt, $sql)) {

    // Bind the input parameters to the query 
    mysqli_stmt_bind_param($stmt, 's', $user_name);

    // execute query 
    mysqli_stmt_execute($stmt);

    // bind the results to variables
    mysqli_stmt_bind_result($stmt, $subscription_id);

    // fetch values
    mysqli_stmt_fetch($stmt); 
    
    // close statement 
    mysqli_stmt_close($stmt);

} else {
    $message = "Error retrieving transaction record: " . mysqli_error($mysqli);
    mysqli_close($mysqli);
    echo json_encode(array('result'=>'error', 'errorFound'=>$message));
    die();
}
 
//echo json_encode(array('result'=>'success', 'message'=>$user_name));

//echo json_encode(array('result'=>'success', 'message'=>$subscription_id));

// now that we have the transaction array, use it to cancel the subscription with Braintree
$result = Braintree_Subscription::cancel($subscription_id);
 
if ($result->success) {

    // the cancellation was successful
    // update user record to show their status as introductory
    $sql = "UPDATE users
            SET member_type='introductory'
            WHERE user_name='$user_name'";
    
    if (!mysqli_query($mysqli, $sql)) {
        mysqli_close($mysqli);
        $message = "Error updating user record: " . mysqli_error($mysqli);
        echo json_encode(array('result'=>'error', 'errorFound'=>$message));
        die();
    }

    // close the connection
    mysqli_close($mysqli);

    // check the array that was returned to make sure it reflects the correct status
    $cancellationStatus = $result->subscription->statusHistory[0]['status'];
    //echo $cancellationStatus;

    if ($cancellationStatus == 'Canceled') {
        echo json_encode(array('result'=>'success', 'array'=>$result));
    } else {
        echo json_encode(array('result'=>'failed', 'array'=>$cancellationStatus));
    }
    
    //echo("Subscription ID: " . $transactionArray->subscription->id . " cancelled<br />");
    //echo json_encode(array('result'=>'success', 'array'=>$result));
        
} else {
    $message;
    foreach ($result->errors->deepAll() as $error) {
        $this_error = ($error->code . ": " . $error->message . "<br />");
        $message .= $this_error;
    }
        
    // close the connection
    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'failure', 'errorFound'=>$message));
}
 
?>
