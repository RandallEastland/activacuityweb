<?php

function update_user_record($user_name, $customer_id, $subscription_id) {

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    // the credit card sale posted. Update the user to 'paid' status
    $sql = "UPDATE users
           SET member_type='paid'
           WHERE user_name='$user_name'";
    
    if (!mysqli_query($mysqli, $sql)) {
        $message = "Error updating record: " . mysqli_error($mysqli);
        return json_encode(array('result'=>'error', 'errorFound'=>$message));
    }

    // increment the number of paid users to keep track of when the introductory
    // pricing option expires
    $sql = "UPDATE num_promotional_users
            SET promo_user_count = promo_user_count + 1";

    if (!mysqli_query($mysqli, $sql)) {
        $message = "Error incrementing user count record: " . mysqli_error($mysqli);
        return json_encode(array('result'=>'error', 'errorFound'=>$message));
    }

    // add the customer id stored with Braintree so that we can refer
    // to it later (e.g. for cancellation)
    $sql = "UPDATE users 
            SET customer_id=?, subscription_id=? 
            WHERE user_name=?";
    $stmt = mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        // Bind the input parameters to the query 
        mysqli_stmt_bind_param($stmt, 'sss', 
            $customer_id, $subscription_id, $user_name);

        // execute query 
        mysqli_stmt_execute($stmt);

        // close statement 
        mysqli_stmt_close($stmt);

    } else {
        mysqli_close($mysqli);
        $message = "Error updating record: " . mysqli_error($mysqli);
        return json_encode(array('result'=>'error', 'errorFound'=>$message));
    }
        
    mysqli_close($mysqli);

    return json_encode(array('result'=>'success'));
}

?>
