<?php

error_reporting(E_ERROR | E_PARSE);
$key = file_get_contents("key.file");

$plaintext = file_get_contents("php://input");
// echo $plaintext . "\n\n";
 
$serialized = serialize($plaintext);
// echo $serialized . "\n\n";

$ciphertext = base64_encode(openssl_encrypt($serialized, 'aes-128-cbc', $key));
echo $ciphertext;

$decrypted = unserialize(openssl_decrypt(base64_decode($ciphertext), 'aes-128-cbc', $key));
// echo $decrypted . "\n\n";
// var_export($decrypted, true)

$jObj = json_decode($decrypted);
// echo var_export($jObj, true) . "\n\n";

$user_name = htmlspecialchars(strip_tags($jObj->subscription->customer->username), ENT_QUOTES);
// echo $user_name;
?>