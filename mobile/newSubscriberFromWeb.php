<?php

// this script receives HTTP requests related to user subscription updates from the ActivAcuity web site.
// it decrypts the request and then hands that request off to the appropriate script (add, renew, cancel).
// For subscriptions that come directly from the mobile app, see 'create_user.php'

$path = $_SERVER['DOCUMENT_ROOT'];

$connection 	  = $path . '/scripts/connection.php';
$newSubscriber 	  = $path . '/mobile/addNewSubscriber.php';
$renewSubscriber  = $path . '/mobile/renewSubscriber.php';
$cancelSubscriber = $path . '/mobile/cancelSubscriber.php';
$notifier 		  = $path . '/mobile/emailUponFailure.php';
$getRaceId		  = $path . '/mobile/getRaceId.php';

include ($connection);
include ($newSubscriber);
include ($renewSubscriber);
include ($cancelSubscriber);
include ($notifier);
include ($getRaceId);

date_default_timezone_set('GMT');
$timestamp = date("Y-m-d H:i:s");

$logging = true;

if ($logging) {
	$log = 'subscriptionResultFromWeb.txt';
	$line_separator ="===============================================================================================\n";
	error_log($line_separator, 3, $log);
	error_log("Attempting to update user subscription record.\nTimestamp:" . $timestamp . "\n", 3, $log);
	error_log($line_separator . "\n", 3, $log);
}

// read the key from a file
$key = file_get_contents("key.file");

$rawString = stripslashes(trim(file_get_contents("php://input"), '"'));

// $rawString = stripslashes(trim(file_get_contents("testinput.txt"), '"'));

if ($logging) {
	error_log("RAW TEXT RECEIVED:\n", 3, $log);
	error_log($rawString, 3, $log);
	error_log("\n\n", 3, $log);
	
	// save the encrypted data string to a file for later testing
	$fp = fopen('ciphertext.txt', 'w');
	fwrite($fp, $rawString);
	fclose($fp);
}

// get the ciphertext from the POST parameter
$jObj = json_decode($rawString);
$ciphertext = $jObj->ciphertext;

if ($logging) {
	error_log("CIPHERTEXT RECEIVED:\n", 3, $log);
	error_log($ciphertext . "\n\n", 3, $log);
}

// need to remove the quotation marks that wrap the ciphertext
$ciphertext = str_replace("%22", "", $ciphertext);

for ($i=1; $i<=2; $i++) {
	$decrypted = unserialize(openssl_decrypt(base64_decode($ciphertext), 'aes-128-cbc', $key));
	if ($logging) error_log("Decryption attempt #" . $i . " was ", 3, $log);
	
	if ($decrypted) {
		if ($logging) {
			error_log("successful.\n\n", 3, $log);
			error_log("DECRYPTED RESULT\n", 3, $log);
			error_log(var_export($decrypted, true), 3, $log);
		}
		break;
	
	} else {
	
		if ($i<2) {
			if ($logging) error_log("NOT successful. Trying again now...\n", 3, $log);
		} else {
			if ($logging) {
				error_log("NOT successful. No further attempts will be made\n", 3, $log);
				error_log("DECRYPTION FAILED - ABORTING SCRIPT PREMATURELY\n", 3, $log);
				error_log($line_separator . $line_separator . "\n\n", 3, $log);
			}
			sendEmailUponFailure();
			die();
		}
	}
}

/*
highlight_string("<?php\n\$data =\n" . var_export($decrypted, true) . ";\n?>");
*/

if ($logging) error_log("\n\n", 3, $log);

// for data from Erin
$user_name 			  = htmlspecialchars(strip_tags($decrypted['subscription']['customer']['email']), ENT_QUOTES);
$status    			  = strip_tags($decrypted['subscription']['status']);
$woo_commerce_race_id = htmlspecialchars(strip_tags($decrypted['subscription']['line_items'][0]['product_id']), ENT_QUOTES);

if (!$woo_commerce_race_id)
	$race_id = 0;
else if (strlen($woo_commerce_race_id) > 0)
	$race_id = getRaceId($mysqli, $woo_commerce_race_id);
else
	$race_id = 0;

// strip whitespace
$user_name = preg_replace('/\s+/', '', $user_name);

// verify username availability
$sql = "SELECT user_id 
		FROM users 
		WHERE user_name='$user_name' AND race_id=$race_id";
	
$result = mysqli_query($mysqli, $sql);

if (!$result) {
	if ($logging) error_log("error determining user name availability\naborting\n", 3, $log);
    echo json_encode(array('result'=>'failure', 'message'=>'error determining user name availability: ' . mysqli_error($result)));
    mysqli_close($mysqli);
    die();
}

if ($logging) error_log("BEGINNING DATABASE UPDATE ATTEMPT\n", 3, $log);
if (mysqli_num_rows($result) > 0) { // indicates a username that exists in the database
	
	if ($status == 'active') { // three possible values: active, cancelled, suspended
		// active status means that this is a renewal
		$updated = renewSubscriber($mysqli, $decrypted, $log, $logging);
	} else { // cancelled or suspended
		$updated = cancelSubscriber($mysqli, $decrypted, $log, $logging);
	}
	
} else { // username does not exist in the database -> add it
	$updated = addNewSubscriber($mysqli, $decrypted, $race_id, $log, $logging);
}
if ($logging) error_log("DONE WITH DATABASE UPDATE ATTEMPT\n\n", 3, $log);

mysqli_close($mysqli);

if ($updated) {
	if ($logging) error_log("Script completed SUCCESSFULLY\n\n", 3, $log);
	echo json_encode(array('result'=>'success', 'message'=>'successfully updated user record'));
} else { 
	if ($logging) error_log("Script FAILED to complete successfully\n\n", 3, $log);
	echo json_encode(array('result'=>'failure', 'message'=>'an error occured'));
}

if ($logging) {
	error_log("END OF UPDATE ATTEMPT\n", 3, $log);
	error_log($line_separator . $line_separator . "\n\n", 3, $log);
}
?>