<?php

/* this script is called by the mobile app to reflect a new or renewed subscription handled by the
	 apple billing service
*/

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$user_name 		 = strip_tags($_POST['user_name']);
$expiration_date = strip_tags($_POST['expiration_date']);
$sale_type		 = strip_tags($_POST['sale_type']);

// $formatted_date = date_format(date_create_from_format('m-d-y', $expiration_date), 'Y-m-d');
$last_renewal_date = date("Y-m-d");
$updated_at = date("Y-m-d");

// these transactions go through Apple's billing service 
$subscription_source = 'Apple';

$logging = true;

if ($logging) {
	$log = 'subscriptionResultFromApp.txt';
	$line_separator ="===============================================================================================\n";
	error_log($line_separator, 3, $log);
	error_log("Attempting to update user subscription record.\n", 3, $log);
	error_log("Timestamp:" . $timestamp . "\n", 3, $log);
	error_log("For user name: " . $user_name . "\n", 3, $log);
	error_log("With expiration date: " . $expiration_date . "\n", 3, $log);
	error_log("Sale Type: " . $sale_type . "\n", 3, $log);
	error_log($line_separator . "\n", 3, $log);
}

// the credit card sale posted. Update the user to 'paid' status
$sql = "UPDATE users
        SET member_type = 'paid', 
			expiration_date = '$expiration_date',
			last_renewal_date = '$last_renewal_date',
			updated_at = '$updated_at',
			subscription_source = '$subscription_source'
        WHERE user_name='$user_name'";

if (!mysqli_query($mysqli, $sql)) {
   $message = "Error updating record: " . mysqli_error($mysqli);
   echo json_encode(array('result'=>'failure', 'errorFound'=>$message));
   
   if ($logging) {
		error_log("Result: FAILED\nError Updating Record: " . $message . "\n", 3, $log);
		error_log("END OF UPDATE ATTEMPT\n", 3, $log);
		error_log($line_separator . $line_separator . "\n\n", 3, $log);
   }
   
   mysqli_close($mysqli);
   die();
}

if ($sale_type == 'new') {
	// increment the number of paid users to keep track of when the introductory
	// pricing option expires
	$sql = "UPDATE num_promotional_users
	        SET promo_user_count = promo_user_count + 1";

	if (!mysqli_query($mysqli, $sql)) {
	    $message = "Error incrementing user count record: " . mysqli_error($mysqli);
	    echo json_encode(array('result'=>'failure', 'errorFound'=>$message));

	    if ($logging) {
			error_log("Result: FAILED\nError Incrementing User Count Record: " . $message . "\n", 3, $log);
			error_log("END OF UPDATE ATTEMPT\n", 3, $log);
			error_log($line_separator . $line_separator . "\n\n", 3, $log);
	    }

		mysqli_close($mysqli);
		die();
	}
}

mysqli_close($mysqli);

echo json_encode(array('result'=>'success'));

if ($logging) {
	error_log("Successfully Updated Record\n", 3, $log);
	error_log("END OF UPDATE ATTEMPT\n", 3, $log);
	error_log($line_separator . $line_separator . "\n\n", 3, $log);
}

?>