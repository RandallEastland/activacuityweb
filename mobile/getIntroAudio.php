<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //get information on this location from the database
    $sql = "SELECT * 
            FROM intro_audio
            ORDER BY filename";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve intro audio list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {
        
        $this_audio_array = array(
            'day_num'     => $row['day_num'],
            'filename'    => $row['filename'],
            'num_seconds' => $row['num_seconds']
        );

        array_push($array, $this_audio_array);
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
?>
