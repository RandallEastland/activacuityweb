<?php

// note that this gets called by 'newSubscriberFromWeb.php'. It is for communicating subscription changes that are
// made via the the ActivAcuity web site (not directly within the app)

function renewSubscriber($mysqli, $data, $log, $logging) {
		
	if ($logging) error_log("This is an attempt to RENEW an EXISTING subscriber\n", 3, $log);
	
	$user_name  		= htmlspecialchars(strip_tags($data['subscription']['customer']['email']), ENT_QUOTES);
	$expiration_date 	= strip_tags($data['subscription']['billing_schedule']['end_at']);
	$updated_at  		= strip_tags($data['subscription']['updated_at']);
	$member_type 		= "paid"; // mark as paid in the event of strange edge cases where the user type might be otherwise
	
	// strip whitespace
	$user_name = preg_replace('/\s+/', '', $user_name);
	if ($logging) error_log("with user name: " . $user_name . "\n", 3, $log);

	// the JSON object is inconsisent with which KV pair it stores the expiration date. If it is missing the 'end_at' key,
	// then it should have a 'next_payment_at' key. However, in this case, we need to add 1 day to determine the expiration date.
	if (strlen($expiration_date) == 0) {
		$expiration_date = strip_tags($data['subscription']['billing_schedule']['next_payment_at']);
	}
		
	//convert to DateTime
	$expiration_date = strtotime('+1 day', strtotime($expiration_date)) ;
	$expiration_date = date('Y-m-d', $expiration_date);
	
	$updated_at = date('Y-m-d', strtotime($updated_at));
	
	// if the expiration date is still empty abort.
	if (strlen($expiration_date) == 0) {
		if ($logging) error_log("No expiration date in either 'end_at' or in 'next_payment_at'\nABORTING ATTEMPT TO UPDATE DATABASE\n", 3, $log);
		return FALSE;
	}
	
	$sql = "UPDATE users
			SET expiration_date=?, updated_at=?, member_type=?
			WHERE user_name=?";
			
	$stmt =  mysqli_stmt_init($mysqli);

	if (mysqli_stmt_prepare($stmt, $sql)) {

	    /* Bind the input parameters to the query */
	    mysqli_stmt_bind_param($stmt, 'ssss', $expiration_date, $updated_at, $member_type, $user_name);

	    /* execute query */
	    if (!mysqli_stmt_execute($stmt)) {
			if ($logging) error_log("SQL update FAILED in renewSubscriber\n", 3, $log);
	        mysqli_stmt_close($stmt);
			return false;
	    }

	    /* close statement */
	    mysqli_stmt_close($stmt);
	}
	
	if ($logging) error_log("SQL update successful\n", 3, $log);
	return true;
}
?>