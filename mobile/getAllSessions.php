<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    $user_name = strtolower(strip_tags($_POST['user_name']));
    
    // if the user name is empty, just pull all the sessions
    // and show no progress
    $sql;

    if (strlen($user_name) == 0) 
    {
        $sql = "SELECT *, 0 AS has_completed
                FROM sessions 
                ORDER BY prog_id, session_num";
    } 
    else 
    {
        $sql = "SELECT sessions.*, 
                    CASE WHEN my_progress.session_id IS NULL
                        THEN 0 ELSE 1 END
                        AS has_completed 
                FROM sessions
                    LEFT JOIN 
                    (
                        SELECT * FROM progress 
                        WHERE user_name = ?
                    ) my_progress
                    USING (session_id)
                ORDER BY prog_id, session_num";
    }

    $array = array();
    if ($stmt = mysqli_prepare($mysqli, $sql)) { 

        /* Bind the input parameters to the query */
        if (strlen($user_name) > 0)
            mysqli_stmt_bind_param($stmt, 's', $user_name);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* bind result variables */
        mysqli_stmt_bind_result($stmt, 
            $session_id, $prog_id, $session_num, $media_file, $play_seconds, $has_completed); 
        
        /* fetch value */
        while (mysqli_stmt_fetch($stmt)) {
            $this_session_array = array(
                'session_id'    => $session_id,
                'prog_id'       => $prog_id,
                'session_num'   => $session_num,
                'media_file'    => $media_file,
                'play_seconds'  => $play_seconds,
                'has_completed' => $has_completed
            );
        
            array_push($array, $this_session_array);
        }
    }

    /* close statement */
    mysqli_stmt_close($stmt);
    
    mysqli_close($mysqli);

    echo json_encode(array('result'=>'success', 'message'=>$array));
    
?>
