<?php

// note that this gets called by 'newSubscriberFromWeb.php'. It is for communicating subscription changes that are
// made via the the ActivAcuity web site (not directly within the app)

function addNewSubscriber($mysqli, $data, $race_id, $log, $logging) {
	
	if ($logging) error_log("This is an attempt to ADD a NEW subscriber\n", 3, $log);
	
	$user_id      		  = htmlspecialchars(strip_tags($data['subscription']['customer']['id']), ENT_QUOTES);
	$user_name  		  = htmlspecialchars(strip_tags($data['subscription']['customer']['email']), ENT_QUOTES);
	$first_name 		  = htmlspecialchars(strip_tags($data['subscription']['customer']['first_name']), ENT_QUOTES);
	$last_name  		  = htmlspecialchars(strip_tags($data['subscription']['customer']['last_name']), ENT_QUOTES);
	$email      		  = strip_tags($data['subscription']['customer']['email']);
	$street     		  = htmlspecialchars(strip_tags($data['subscription']['customer']['billing_address']['address_1']), ENT_QUOTES);
	$city				  = htmlspecialchars(strip_tags($data['subscription']['customer']['billing_address']['city']), ENT_QUOTES);
	$state				  = strip_tags($data['subscription']['customer']['billing_address']['state']);
	$password   		  = htmlspecialchars_decode(strip_tags($data['subscription']['account_password']));
	$device_salt		  = substr(md5(uniqid(rand(), true)), -13);
	$billing    		  = strip_tags($data['subscription']['billing_schedule']['period']);
	$member_type 		  = "paid";
	$last_renewal_date 	  = strip_tags($data['subscription']['customer']['last_order_date']);
	$expiration_date 	  = strip_tags($data['subscription']['billing_schedule']['end_at']);
	$created_at  		  = strip_tags($data['subscription']['created_at']);
	$updated_at  		  = strip_tags($data['subscription']['updated_at']);
	$subscription_source  = 'Woo Commerce';
	$bool_must_reset_pw   = 0;

	if (strlen($race) == 0)
		$race = 'none';
		
	// the JSON object is inconsisent with which KV pair it stores the expiration date. If it is missing the 'end_at' key,
	// then it should have a 'next_payment_at' key. However, in this case, we need to add 1 day to determine the expiration date.
	if (strlen($expiration_date) == 0)
		$expiration_date = strip_tags($data['subscription']['billing_schedule']['next_payment_at']);
	
	// echo $user_id . "\n";
	// echo $user_name . "\n";
	// echo $first_name . "\n";
	// echo $last_name . "\n";
	// echo $email . "\n";
	// echo $street . "\n";
	// echo $city . "\n";
	// echo $state . "\n";
	// echo $password . "\n";
	// echo "device salt: " . $device_salt . "\n";
	// echo $billing . "\n";
	// echo "last: " . $last_renewal_date . "\n";
	// echo "expiration: " . $expiration_date . "\n";
	// echo "created: " . $created_at . "\n";
	// echo "updated: " . $updated_at . "\n";

	// strip whitespace
	$user_name = preg_replace('/\s+/', '', $user_name);
	$email = preg_replace('/\s+/', '', $email);
	$password = preg_replace('/\s+/', '', $password);

	if ($logging) error_log("with user name: " . $user_name . "\n", 3, $log);
	
	// if the expiration date is still empty abort.
	if (strlen($expiration_date) == 0) {
		if ($logging) error_log("No expiration date in either 'end_at' or in 'next_payment_at'\nABORTING ATTEMPT TO UPDATE DATABASE\n", 3, $log);
		return FALSE;
	}
	
	//convert to DateTime
	$expiration_date = strtotime('+1 day', strtotime($expiration_date)) ;
	$expiration_date = date('Y-m-d', $expiration_date);
	
	$last_renewal_date = date('Y-m-d', strtotime($last_renewal_date));
	$created_at = date('Y-m-d', strtotime($created_at));
	$updated_at = date('Y-m-d', strtotime($updated_at));
	
	// the app hashes the plaintext password before it sends it across the internet for further processing by the server
	// we have to mirror that process here as if we were about to send it over HTTP so that when the user logs in from the app
	// the same final hashed value will result. This is the password that gets will get sent by the app to the server for further
	// hashing
	$md5 = md5($password);
	$concat = $user_name . $md5 . $device_salt;
	$app_hash = hash('sha256', $concat);

	// now use that hashed value as what the server will receive. When the user actually attempts to log in from the app,
	// the server will take that value and do some further hashing to evaluate what is saved in the database.
	$salt = uniqid(); // Could use the second parameter to give it more entropy.
	$algo = '6'; // This is CRYPT_SHA512 as shown on http://php.net/crypt
	$rounds = '5000'; // The more, the more secure it is!
	$cryptSalt = '$' . $algo . '$rounds=' . $rounds . '$' . $salt; // This is the "salt" string we give to crypt().
	$hashedPassword = crypt($app_hash, $cryptSalt);
	
	// echo "cryptSalt: " . $cryptSalt . "\n";
	// echo "hashedPassword: " . $hashedPassword . "\n";

	$sql = "INSERT INTO users
			(user_id, race_id, user_name, first_name, last_name, email, street, city, st, salt, hashed_password, device_salt,
			 member_type, last_renewal_date, expiration_date, created_at, updated_at, bool_must_reset_pw, subscription_source)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	$stmt =  mysqli_stmt_init($mysqli);

	if (mysqli_stmt_prepare($stmt, $sql)) {

	    /* Bind the input parameters to the query */
	    mysqli_stmt_bind_param($stmt, 'iisssssssssssssssis', $user_id, $race_id, $user_name, $first_name, $last_name, $email, $street, $city, $state,
									$cryptSalt, $hashedPassword, $device_salt, $member_type, $last_renewal_date, $expiration_date, $created_at,
									$updated_at, $bool_must_reset_pw, $subscription_source);

	    /* execute query */
	    mysqli_stmt_execute($stmt);

	    if (mysqli_stmt_affected_rows($stmt) != 1) {
			if ($logging) error_log("SQL insert FAILED", 3, $log);
	        mysqli_stmt_close($stmt);
			return false;
	    }

	    /* close statement */
	    mysqli_stmt_close($stmt);
	}
	
	if ($logging) error_log("SQL insert successful\n", 3, $log);
	return true;
}
?>