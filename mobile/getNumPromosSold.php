<?php

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$sql = "SELECT promo_user_count, max_promos
        FROM num_promotional_users
        LIMIT 1";
		
$result = mysqli_query($mysqli, $sql);
$row = mysqli_fetch_array($result);

echo json_encode(array(
    'result'     => 'success',
    'count'      => $row[0],
    'max_promos' => $row[1]
));

?>
