<?php

function create_customer($customer) {

    // first check that the customer exists
    // if it does, the create() function won't work, so we have to 
    // get the customer token from the existing record
    try {
        $existingCustomer = Braintree_Customer::find($customer['id']);
        $preg = "/token=([^,]+)/i";
        preg_match($preg, $existingCustomer, $token);
        return $token[1];

    } catch (Exception $e) { 
        // failure to find the customer will throw an error
        // no need to do anything, just move on
    }

    // the customer doesn't exist, so we must create a new record
    $result = Braintree_Customer::create($customer);

    if ($result->success) {
        //return $result->customer->id;
        return $result->customer->creditCards[0]->token;
    } else {
        foreach($result->errors->deepAll() AS $error) {
            return -1;
            //echo($error->code . ": " . $error->message . "\n");
            break;
        }
    }
     
}

?>
