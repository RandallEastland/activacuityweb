<?php

function getPromoStatus($mysqli) {
	$sql = "SELECT promo_user_count, max_promos
	        FROM num_promotional_users
	        LIMIT 1";
		
	$result = mysqli_query($mysqli, $sql);
	$row = mysqli_fetch_array($result);

	return array('promos_sold' => $row[0], 'promos_allowed' => $row[1]);
}

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$promo_status = getPromoStatus($mysqli);
$promos_sold    = $promo_status['promos_sold'];
$promos_allowed = $promo_status['promos_allowed'];

//////////////////////////////////////////////////////////////////////////////
// Use these for production	
$monthly_id = 'com.activacuity.monthly';

if ($promos_sold < $promos_allowed)
	$annual_id = 'com.activacuity.annual.early_adopter';
else
	$annual_id = 'com.activacuity.annual.standard';

//////////////////////////////////////////////////////////////////////////////
// Use these for sandboxing
// $monthly_id = 'com.mobilenicity.consumable.test.weekly';
// $annual_id = 'com.mobilenicity.consumable.test.monthly';
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Everything below should not need to be changed
$sql = "SELECT product_id, price
        FROM apple_product_ids
		WHERE product_id='$monthly_id' OR product_id='$annual_id'"; 
           
$result = mysqli_query($mysqli, $sql);

if(!$result) {
    mysqli_close($mysqli);
    echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve product ids'));
    die();
}
    
$array = array();
while ($row = mysqli_fetch_array($result)) {
	if ($row['product_id'] == $monthly_id) {
		$this_product = array('renewal'=>'monthly', 'product_id'=>$row['product_id'], 'price'=>$row['price']);
	} else {
		$this_product = array('renewal'=>'annual', 'product_id'=>$row['product_id'], 'price'=>$row['price']);		
	}

    array_push($array, $this_product);
}

mysqli_close($mysqli);

echo json_encode(array('result'=>'success', 'message'=>$array));
?>