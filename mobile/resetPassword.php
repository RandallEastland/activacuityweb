<?php

/* TO DO: 
 * - add this application to google developer account (if necessary later)
 * - separate this function into its own file
 */

function send_confirmation($mysqli, $temp_password, $first_name, $last_name, $email) {
    
    if (!class_exists("php_mailer")) {
        require_once('../scripts/PHPMailer/class.phpmailer.php');
    }

    //set up the PHP mailer
    $mail = new PHPMailer();
    $mail->IsSMTP();  //telling the class to use SMTP
    $mail->SMTPDebug = 0; // >debugging
    // $mail->SMTPDebug = 1; // errors and messages
    //$mail->SMTPDebug = 2; // messages only

    $mail->isHTML(true);
    $mail->Host         = "mail.activacuity.com";
    $mail->WordWrap     = 50;
    $mail->SMTPAuth     = false;
    // $mail->SMTPSecure   = "ssl";
    $mail->Port         = 26;
    $mail->Username     = "support@activacuity.com";
    $mail->Password     = "28iT42R6T7hC!";
    $mail->Subject      = "ActivAcuity Mobile App Password Reset";    
    $mail->SetFrom("support@activacuity.com", "Terry Chiplin");
    $mail->AddReplyTo("support@activacuity.com", "Terry Chiplin");
	
    $to = $first_name . " " . $last_name;
    $mail->AddAddress($email, $to);
    $sender = "Terry Chiplin";
    
    //build message body
    $twolinebreak = "<br/>";

    $body  = "Hello " . $first_name . "," . $twolinebreak . $twolinebreak;

    $body .= "Sorry to hear that you are having troubles with your password. ";
    $body .= "In response to your request, we have reset the password to your ";
    $body .= "ActivAcuity mobile application.";
    $body .= $twolinebreak . $twolinebreak;
    $body .= "A temporary password has been issued and will be valid for 24 hours.  ";
    $body .= "Please log in within this time frame at which time you will be prompted ";
    $body .= "to enter a permanent password of your choice.";
    $body .= $twolinebreak . $twolinebreak;

    $body .= "Your temporary password is:  ";
    $body .= $temp_password . $twolinebreak . $twolinebreak;
    $body .= "Thanks for using ActivAcuity!";
    $body .= $twolinebreak . $twolinebreak;

    $mail->Body = $body;

    if (!$mail->Send()) {
        return "error: email not sent: " . $mail->ErrorInfo;
    }

    //confirmation email sent successfully
    return true;
}

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

require('../scripts/PHPMailer/class.phpmailer.php');

$user_name = $_POST['user_name'];

$temp_password = rand(1, 10000);

$sql = "SELECT first_name, last_name, email 
        FROM users 
        WHERE user_name='$user_name' 
        LIMIT 1";

$result = mysqli_query($mysqli, $sql);
if (!result) {
    echo json_encode(array('result'=>'failure','message'=>'failed to retrieve user record'));
    mysqli_close($mysqli);
    die();
}

if (mysqli_num_rows($result) == 0) {
    echo json_encode(array('result'=>'no_match','message'=>'no matching user record found for ' . $user_name));
    mysqli_close($mysqli);
    die();
}

$user = mysqli_fetch_array($result, MYSQL_ASSOC);
$first_name = $user['first_name'];
$last_name  = $user['last_name'];
$email      = $user['email'];

//Hash the temporary passsword.
$salt = uniqid();
$algo = '6'; // This is CRYPT_SHA512 as shown on http://php.net/crypt
$rounds = '5000';
$cryptSalt = '$' . $algo . '$rounds=' . $rounds . '$' . $salt; // This is the "salt" string we give to crypt().
$hashedPassword = crypt($temp_password, $cryptSalt);

$updated_at = date('Y-m-d H:i:s');

$sql = "UPDATE users
        SET hashed_password='$hashedPassword', salt='$cryptSalt', device_salt='', 
            bool_must_reset_pw='1', updated_at='$updated_at' 
            WHERE user_name='$user_name' ";

$result = mysqli_query($mysqli, $sql);

if (!$result) {
    echo json_encode(array('result'=>'failure', 'message'=>'failed to update user record'));
    mysqli_close($mysqli);
    die();
}

$confirmed = send_confirmation($mysqli, $temp_password, $first_name, $last_name, $email);
if ($confirmed != true) {
    echo json_encode(array('result'=>'failure', 'message'=>$confirmed));
    mysqli_close($mysqli);
    die();
}

mysqli_close($mysqli);

echo json_encode(array('result'=>'success', 'message'=>'password has been reset: '.$temp_password));
    
?>
