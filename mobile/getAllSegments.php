<?php
/*
    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //get information on this location from the database
    $sql = "SELECT * FROM segments ORDER BY segment_num";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve segments list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {
        $this_segment_array = array(
            'segment_id'    => $row['segment_id'],
            'segment_num'   => $row['segment_num'],
            'segment_name'  => $row['segment_name'],
            'display_color' => $row['display_color']
        );

        array_push($array, $this_segment_array);
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
*/


$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$user_name = strtolower(strip_tags($_POST['user_name']));

$sql = "SELECT race_id FROM users WHERE user_name='$user_name'";
$result = mysqli_query($mysqli, $sql);

if(!$result) {
    echo json_encode(array('result'=>'failure', 'message'=>'cannot find user name'));
    die();
}

$user = mysqli_fetch_array($result);
$race_id = $user['race_id'];

//get information on this location from the database

if ($race_id == "") {
    $sql = "SELECT * 
			FROM segments
		 	WHERE race_id=0
			ORDER BY segment_num";
} else {
    $sql = "SELECT * 
			FROM segments
		 	WHERE race_id=0 OR race_id=$race_id
			ORDER BY segment_num";	
}

$result = mysqli_query($mysqli, $sql);

if(!$result) {
    mysqli_close($mysqli);
    echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve segments list'));
    die();
}
    
$array = array();
while ($row = mysqli_fetch_array($result)) {
    $this_segment_array = array(
        'segment_id'    => $row['segment_id'],
        'segment_num'   => $row['segment_num'],
        'segment_name'  => $row['segment_name'],
        'display_color' => $row['display_color']
    );

    array_push($array, $this_segment_array);
}

mysqli_close($mysqli);

echo json_encode(array('result'=>'success', 'message'=>$array));

?>
