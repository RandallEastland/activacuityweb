<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //get information on this location from the database
    $sql = "SELECT * FROM programs ORDER BY segment_id, prog_num";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve programs list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {
        $this_program_array = array(
            'prog_id'       => $row['prog_id'],
            'segment_id'    => $row['segment_id'],
            'prog_num'      => $row['prog_num'],
            'description'   => $row['description']
        );

        array_push($array, $this_program_array);
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
?>
