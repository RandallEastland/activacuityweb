<?php

    function encodeJSONObj($tag_result, $tag_message) {
        //Return information to the app
        $array = array(
            'result'  => $tag_result, 
            'message' => $tag_message
        );

        return json_encode($array);
    }

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    $user_name = strip_tags($_POST['user_name']);
    
    //strip out whitespace and any html tags
    $user_name = preg_replace('/\s+/', '', $user_name);

    if ($user_name == "") {
        echo encodeJSONObj ("failure", "Please enter your user name");
        die();
    }
    
    //get salt value for this user from the database
    $sql = "SELECT device_salt FROM users WHERE user_name=? LIMIT 1";

    if ($stmt = mysqli_prepare($mysqli, $sql)) { 

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 's', $user_name);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* store result (only necessary for the mysqli_stmt_num_rows command) */
        mysqli_stmt_store_result($stmt);

        // make sure that the result set is not empty
        if (mysqli_stmt_num_rows($stmt) == 0) {
            mysqli_stmt_close($stmt);
            mysqli_close($mysqli);
            $tag_result = "failure";
            $tag_message = "no matching athlete";
            echo encodeJSONObj($tag_result, $tag_message);
            die();
        }
        
        /* bind result variables */
        mysqli_stmt_bind_result($stmt, $device_salt); 
        
        /* fetch value */
        mysqli_stmt_fetch($stmt);

        $tag_result = "success";
        $tag_message = $device_salt;
        echo encodeJSONObj($tag_result, $tag_message);
        
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);

?>
