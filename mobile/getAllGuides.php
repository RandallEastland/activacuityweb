<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    // this query attaches a new field to the table.
    // the contents of that field is an integer representing how many child guides
    // are attached to each 'parent' guide. The app needs this information since
    // it handles parent guides differently.
    $sql = "SELECT guides.*, child_counts.num_children AS num_children
            FROM guides
            INNER JOIN
                (SELECT IFNULL(parents.guide_num, '0') AS guide_num,
                        COUNT(children.guide_num) AS num_children 
                FROM guides parents
                LEFT JOIN guides children
                ON parents.guide_num = children.parent_id
                GROUP BY parents.guide_num)
            AS child_counts
            ON guides.guide_num = child_counts.guide_num
            ORDER BY guides.guide_num"; 
               
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve guides list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {
        $this_guide_array = array(
            'guide_id'     => $row['guide_id'],
            'guide_num'    => $row['guide_num'],
            'num_children' => $row['num_children'],
            'description'  => $row['description'],
            'elaboration'  => $row['elaboration'],
            'parent_id'    => $row['parent_id'],
            'num_children' => $row['num_children']
        );

        array_push($array, $this_guide_array);
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
?>
