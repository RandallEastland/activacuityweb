<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //get information on this location from the database
    $sql = "SELECT * 
            FROM videos
            ORDER BY resolution, day_num";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        mysqli_close($mysqli);
        echo json_encode(array('result'=>'failure', 'message'=>'failed to retrieve video list'));
        die();
    }
        
    $array = array();
    while ($row = mysqli_fetch_array($result)) {
        
        $this_video_array = array(
            'day_num'    => $row['day_num'],
            'resolution' => $row['resolution'],
            'filename'   => $row['filename']
        );

        array_push($array, $this_video_array);
    }

    mysqli_close($mysqli);
    
    echo json_encode(array('result'=>'success', 'message'=>$array));
?>
