<?php
	
function is_race_complete($mysqli, $race_id) {
	
	$sql = "SELECT race_date, expiration_text FROM races WHERE race_id=$race_id LIMIT 1";
	
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        return array('isComplete'=>false);
        die();
    }

    while ($row = mysqli_fetch_array($result)) {
		$race_date = $row['race_date'];
		$expiration_text = $row['expiration_text'];
    }
	
	$seven_days_ago = date('Y-m-d', strtotime("-7 days"));
	
	$is_complete = $race_date < $seven_days_ago;
	
	return array('isComplete'=>$is_complete, 'message'=>$expiration_text);
}

?>