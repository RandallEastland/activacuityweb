<?php

function sendEmailUponFailure() {
	
    $path = $_SERVER['DOCUMENT_ROOT'];
	require ($path . '/scripts/PHPMailer/class.phpmailer.php');

    //set up the PHP mailer through my own email account
    $mail = new PHPMailer();
    $mail->IsSMTP();  //telling the class to use SMTP
    $mail->SMTPDebug = 0; // >debugging
    // $mail->SMTPDebug = 1; // errors and messages
    //$mail->SMTPDebug = 2; // messages only

    $mail->isHTML(true);
    $mail->Host         = "mail.activacuity.com";
    $mail->WordWrap     = 50;
    $mail->SMTPAuth     = true;
    $mail->SMTPSecure   = "ssl";
    $mail->Port         = 465;
    $mail->Username     = "support@activacuity.com";
    $mail->Password     = "28iT42R6T7hC!";
    $mail->Subject      = "ActivAcuity Subscription From Web Failed";

    $mail->SetFrom("support@activacuity.com", "Randy Eastland");
    $mail->AddReplyTo("support@activacuity.com", "Randy Eastland");
    
    $mail->AddAddress("randy.eastland@gmail.com", "Randy Eastland");
    $sender = "Randy Eastland";	

    $body  = "This is a message from yourself. You should check the log file at<br><br>";
	$body .= "http://www.activacuity.com/mobile/subscriptionResultFromWeb.txt<br><br>";
	$body .= "An attempt to update a subscription has failed to decrypt.";
    
    $mail->Body = $body;

    if ($mail->Send()) {
		echo json_encode(array('result'=>'success'));	
    } else {
        echo json_encode(array('result'=>'failure', 'message'=>'email not sent: ' . $mail->ErrorInfo));
    }    
}
?>
