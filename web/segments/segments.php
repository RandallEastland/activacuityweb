<?php
session_start();

// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;

?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Segments</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

        #bodyframe{
            margin:0 auto;
            width:800px;
            float:center;
            clear:left;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

</head>

<body>

    <script language="javascript" type="text/javascript">

        function deleteThis(btn_id) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            var id = btn_id.split('-')[1];

            //confirm intention to delete
            var question = "Are you sure that you want to delete this segment?";            
            if (!confirm(question)) {return;}
	        var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;

                    //check to see if this segment is referred to by a program
                    if (response.split("|")[0] == "dependency") {
                        var numDependencies = response.split("|")[1];
                        var alertText = "Deletion canceled. This segment is referred to by at least ";
                        alertText += numDependencies + " program(s).";
                        alert(alertText);
                        return; 
                    }

                    if (response == "success") {
                        window.location.reload();
                    }
                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/segments/delete_segment.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
 
        function updateThis(id, list, program_number, description, media_url) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = id.split('-')[1];
            
            var next = "update_segment.php?id="+id;
            
            window.location.href = next;
            
        }
        
        function addNewSegment() {
            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            window.location.href = "new_segment.php";
        }
    
    </script>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Segments</h1>
            <p>Use this area to update the list of segments.</p>
        </div>
    </div>
    
    <div id="bodyframe">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="addNewSegment()">Add New Segment</button>          
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>


        <table id="dataset">
            <tr class="dataheader" valign='bottom'>
                <td align='center'>Segment Number</td>
                <td>Segment Name</td>
                <td>Display Color</td>
				<td>Race Affiliation</td>
                <td align='center'>+</td>
                <td align='center'>-</td>
            </tr>

            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $connection = $path . '/scripts/connection.php';
                include ($connection);

                $sql="SELECT segment_id, segment_num, segment_name, display_color, 
							 races.race_id as race_id, races.race_name AS race_name
					  FROM segments 
					  JOIN races ON segments.race_id = races.race_id";
                
				$result = mysqli_query($mysqli, $sql);
                if (!$result) {die("SQL error retrieving segment lists.");}

                if (mysqli_num_rows($result) == 0) {
                    echo "
                        The list of segments is empty.

                        ";
                    die();
                }
                    
                while ($row = mysqli_fetch_array($result)) {

                    $segment_num   = $row['segment_num'];
                    $segment_name  = $row['segment_name'];
                    $display_color = $row['display_color'];
					$race_id	   = $row['race_id'];
					$race_name	   = $row['race_name'];
                    
                    //make unique names for each <td> in this row
                    $segment_id  = $row['segment_id']; //this is the id for this segment in the database 
                    
                    $id_hdr            = "id_hdr-"            . $segment_id;
                    $segment_num_hdr   = "segment_num_hdr-"   . $segment_id;
                    $segment_name_hdr  = "segment_name_hdr-"  . $segment_id;
                    $display_color_hdr = "display_color_hdr-" . $segment_id;
					$race_id_hdr	   = "race_id_hdr-"		  . $segment_id;
					$race_name_hdr	   = "race_name_hdr-"	  . $segment_id;
                    $btn_update        = "btn_update-"        . $segment_id;
                    $btn_delete        = "btn_delete-"        . $segment_id;
                    
                    //Build this row of the table
                    $tableString = 
                        "<tr class='datarow'>"  .  
                            "<td class='id'         id='$id_hdr' style='display:none'>" 	 . $segment_id    . "</td>" .
                            "<td class='xxx_center' id='$segment_num_hdr'>"             	 . $segment_num   . "</td>" .
                            "<td class='cl'         id='$segment_name_hdr'>"            	 . $segment_name  . "</td>" .
                            "<td class='c0'         id='$display_color_hdr'>"           	 . $display_color . "</td>" .
							"<td class='id'         id='$race_id_hdr' style='display:none'>" . $race_id       . "</td>" .
                            "<td class='cc'         id='$race_name_hdr'>"           	 	 . $race_name	  . "</td>" .
					
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/update.png' id='$btn_update' 
                                    onclick='updateThis(this.id)'>".
                            "</td>" .
                       
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/delete.png' id='$btn_delete' 
                                    onclick='deleteThis(this.id, \"$description\")'>".
                            "</td>" .
                        "</tr>";
                        
                    echo $tableString;
                }    
           
                mysqli_close($mysqli);
            ?>

        </table>

    </div>

</body>

</html>
