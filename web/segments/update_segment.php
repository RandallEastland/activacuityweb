<?php
session_start();

// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Update Segment</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

</head>

<body>

    <script type="text/javascript">
   
        window.onload = function getSegmentInfo() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = document.URL.split('=')[1];
            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = JSON.parse(xmlhttp.responseText);
                    // console.log(response);
                    if (response.result == "success") {
						
                        document.getElementById("segment_num").value   = response.data.segment_num;
                        document.getElementById("segment_name").value  = response.data.segment_name;
                        document.getElementById("display_color").value = response.data.display_color;
						document.getElementById("selected_race").value = response.data.race_id;
						
						var race_list = response.data.race_list;
						var select = document.getElementById("race_list");

						// populate the drop down list with the list of races
						for (var i=0; i<race_list.length; i++) {
							var race = race_list[i];
						    var opt = document.createElement("option");
						    opt.text = race.race_name;
						    opt.value = race.race_id;
						    select.appendChild(opt);
						}
						
						// select the active race id from the drop down list
                        for (var i=0; i<select.options.length; i++){
                            if (select.options[i].value == response.data.race_id) {
                                select.options[i].selected = true;
                                break;
                            }
                        }
                                                
                    } else {
                        document.getElementById("rightsidebox").innerHTML="Failed to retrieve segment information.";
                    }
                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/segments/get_segment.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);
            
            return false;
        }

        function updateSegment() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
             
            var segment_id     = document.URL.split('=')[1];
            var segment_num    = document.getElementById("segment_num").value;
            var segment_name   = document.getElementById("segment_name").value;
            var display_color  = document.getElementById("display_color").value;
			
			var select = document.getElementById("race_list");
			for (var i=0; i<select.options.length; i++) {
				opt = select.options[i];
		        if ( opt.selected === true ) {
					var race_id = opt.value;
					break;
		        }
			}
			
            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response == "success") {
                        //alert("Changes to " + name + " updated successfully");
                        window.location.href = "segments.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "segment_id="     +encodeURIComponent(segment_id)+
                            "&segment_num="   +encodeURIComponent(segment_num)+
                            "&segment_name="  +encodeURIComponent(segment_name)+
                            "&display_color=" +encodeURIComponent(display_color)+
							"&race_id="		  +encodeURIComponent(race_id);
            
            xmlhttp.open("POST", "../scripts/segments/update_segment.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
        
        
        function goToDashboard() {
            window.location.href = "../dashboard.html";
        }
    
    </script>
		
		<div id="selected_race" style="display: none;"></div>

    <div id="titleframe">

        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Update Segment</h1>
            <p>Make any changes that you wish - then click Update Segment</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="updateSegment()">Update Segment</button>
            <button type="submit" class="bigbutton" onClick="parent.location='segments.php'">All Segments</button>
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>

        </div>
        
        <div id="additem" >
                
            <form id="form" name="form" method="post" action="../scripts/create_segment.php">

                <label>Segment Number
                    <span class="small">Assign a number to this segment.</span>
                </label>
                <input type="text" id="segment_num" autofocus />
                
                <label>Segment Name
                    <span class="small">This will appear in the app.</span>
                </label>
                <input type="text" id="segment_name" />
    
                <label>Display Color
                    <span class="small">RGB code to display in the app.</span>
                </label>
                <input type="text" id="display_color" />
				
                <label>Race Affiliation
                    <span class="small">Select the race that this segment applies to.</span>
                </label>
				<select class="combobox" id="race_list">
              
            </form>

        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
