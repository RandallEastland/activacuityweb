<?php

    include('../connection.php');

    $segment_id = $_POST['id'];
    
    //get information on this coach from the database
    $sql = "SELECT * FROM segments WHERE segment_id=$segment_id";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        echo json_encode(array('result'=>'failure'));
        die();
    }

    $program = mysqli_fetch_array($result);
    
	$array = array(
		'segment_id' 	=> $program['segment_id'],
    	'segment_num' 	=> $program['segment_num'],
    	'segment_name'  => $program['segment_name'],
    	'display_color' => $program['display_color'],
		'race_id'		=> $program['race_id']
	);
	
	// now get a list of all races to populate the drop down list
	$sql = "SELECT * FROM races";
	$result=mysqli_query($mysqli, $sql);
	
	if (mysqli_num_rows($result) > 0) {

		$race_array = array();
		
	    while ($row = mysqli_fetch_array($result)) {
	        $this_race = array('race_id' => $row['race_id'], 'race_name' => $row['race_name']);
			array_push($race_array, $this_race);
	    }
		
		$array['race_list'] = $race_array;
	}

    mysqli_close($mysqli);

    echo json_encode(array('result'=>'success', 'data'=>$array));

?>
