<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    //session_start();
     
    //retrieve input values from the form
    $segment_id    = $_POST['segment_id'];
    $segment_num   = strip_tags($_POST['segment_num']);
    $segment_name  = strip_tags($_POST['segment_name']);
    $display_color = strip_tags($_POST['display_color']);
	$race_id	   = strip_tags($_POST['race_id']);

    if ($segment_num == "") {
        echo "Please enter a segment number.";
        die();
    }

    if (!is_numeric($segment_num)) {
        echo "Please enter a numeric value for the segment number.";
        die();
    }
    
    if ($segment_name == "") {
        echo "Please enter a name for this segment.";
        die();
    }
        
    if ($display_color == "") {
        echo "Please enter a color that the app should display this segment in.";
        die();
    }

    $regex = '/^#[A-Fa-f0-9]{6}$/';
    if (!preg_match($regex, $display_color)) {
        echo "Please enter your display color in 6-character hexadecimal notation.";
        die();
    }
    
	$display_color = strtoupper($display_color);
	
    $sql = "UPDATE segments 
            SET segment_num=?, 
                segment_name=?, 
                display_color=?,
				race_id=?
            WHERE segment_id=?";
    $stmt = mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'issii', 
            $segment_num, $segment_name, $display_color, $race_id, $segment_id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);

    //new user created, refresh the session time variable
    $_SESSION['session_time'] = time();

    echo "success";

?>
