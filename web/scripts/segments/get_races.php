<?php

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

$sql = "SELECT * FROM races";
$result=mysqli_query($mysqli, $sql);

if (mysqli_num_rows($result) > 0) {

	$race_array = array();
	
    while ($row = mysqli_fetch_array($result)) {
        $this_race = array('race_id' => $row['race_id'], 'race_name' => $row['race_name']);
		array_push($race_array, $this_race);
    }
	
	$array['race_list'] = $race_array;
}

mysqli_close($mysqli);

echo json_encode(array('result'=>'success', 'data'=>$array));

?>