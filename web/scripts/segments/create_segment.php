<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //retrieve input values from the form
    $segment_num   = strip_tags($_POST['segment_num']);
    $segment_name  = strip_tags($_POST['segment_name']);
    $display_color = strip_tags($_POST['display_color']);
	$race_id	   = strip_tags($_POST['race_id']);
	
    //make sure all fields have been entered
    if ($segment_num == "") {
        echo "Please enter the segment number.";
        die();
    }
        
    if (!is_numeric($segment_num)) {
        echo "Please enter a numeric value for the segment number.";
        die();
    }
    
    if ($segment_name == "") {
        echo "Please enter a name for this segment.";
        die();
    }
    
    if ($display_color == "") {
        echo "Please enter a color for the app to display this segment in.";
        die();
    }
	
	$display_color = strtoupper($display_color);

    $regex = '/^#[A-Fa-f0-9]{6}$/';
    if (!preg_match($regex, $display_color)) {
        echo "Please enter your display color in 6-character hexadecimal notation.";
        die();
    }

    $sql = "INSERT INTO segments (segment_num, segment_name, display_color, race_id)
            VALUES (?,?,?,?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'issi', 
            $segment_num, $segment_name, $display_color, $race_id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting record");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);
    
    $_SESSION['session_time'] = time();

    echo "success";

?>
