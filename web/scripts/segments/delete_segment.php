<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    session_start();
     
    //retrieve input values from the form
    $id = $_POST['id'];

    //check to see if this component is referred to by a Program
    $sql = "SELECT DISTINCT prog_id 
            FROM programs 
            WHERE segment_id = $id";
    $result = mysqli_query($mysqli, $sql);
    $count = mysqli_num_rows($result);
    if ($count > 0) {
        echo "dependency|".$count;
        die();
    }

    $sql = "DELETE FROM segments WHERE segment_id='$id'";
    $result = mysqli_query($mysqli, $sql);

    if (!$result) {
        echo "Error removing segment";
        die();
    }

    //coach successfully deleted, update the session time variable
    $_SESSION['session_time'] = time();

    mysqli_close($mysqli);

    echo "success";

?>
