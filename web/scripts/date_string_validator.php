<?php

function invalid_date($str_date) {
    
    $str_date = strip_tags($str_date);
    $date = date_parse($str_date);

    if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
        return 0; // date is valid
    else
        return 1; // date is invalid
    
}

?>
