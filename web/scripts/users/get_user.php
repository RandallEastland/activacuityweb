<?php

    include('../connection.php');

    $user_id = $_POST['id'];
    
    //get information on this coach from the database
    $sql = "SELECT * FROM users WHERE user_id=$user_id";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        echo json_encode(array('result'=>'failure'));
        die();
    }

    $user = mysqli_fetch_array($result);
    
	$state = $user['st'];
	
	if ($state == "")
		$state = "AL";
	
	$include_in_progress_report = $user['include_in_progress_report'] == "1" ? "Yes" : "No";
		
	$array = array(
	    'user_name'          => $user['user_name'],
	    'first_name'         => $user['first_name'],
	    'last_name'          => $user['last_name'],
	    'street'             => $user['street'],
	    'city'               => $user['city'],
	    'state'              => $state,
	    'member_type'        => $user['member_type'],
		'race_id'			 => $user['race_id'],
	    'last_renewal_date'  => $user['last_renewal_date'],
	    'expiration_date'    => $user['expiration_date'],
		'include_in_progress_report'	=> $include_in_progress_report,
		'report_section'	 => $user['report_section'],
	);

	// now get a list of all races to populate the drop down list
	$sql = "SELECT * FROM races";
	$result=mysqli_query($mysqli, $sql);

	if (mysqli_num_rows($result) > 0) {

		$race_array = array();

	    while ($row = mysqli_fetch_array($result)) {
			$race_name = $row['race_name'];
			
			if ($race_name == 'All Races')
				$race_name = 'ActivAcuity';
			
	        $this_race = array('race_id' => $row['race_id'], 'race_name' => $race_name);
			array_push($race_array, $this_race);
	    }

		$array['race_list'] = $race_array;
	}

    mysqli_close($mysqli);

    echo json_encode(array('result'=>'success', 'data'=>$array));

?>
