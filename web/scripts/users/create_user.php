<?php

/* This script creates new users from www.activacuity.com/users.php. As such, these users do not yet have a paid subscription
	to the app. To see the scripts that create paid subscribers, see:
	- newSubscriberFromWeb.php
    
*/
    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    $date_string_validator = $path . '/scripts/date_string_validator.php';
    include ($connection);
    include ($date_string_validator);
    
    //retrieve input values from the form
    $user_name          = strip_tags($_POST['user_name']);
    $first_name         = strip_tags($_POST['first_name']);
    $last_name          = strip_tags($_POST['last_name']);
    $street             = strip_tags($_POST['street']);
    $city               = strip_tags($_POST['city']);
    $st                 = strip_tags($_POST['st']);
    $member_type        = strip_tags($_POST['member_type']);
    $last_renewal_date  = strip_tags($_POST['last_renewal_date']);
    $expiration_date    = strip_tags($_POST['expiration_date']);
    
    //make sure all fields have been entered
    if ($user_name == "") {
        echo "Please enter the username.";
        die();
    }
        
    if ($first_name == "") {
        echo "Please enter the first name of this user.";
        die();
    }
    
    if ($last_name == "") {
        echo "Please enter the last name of this user.";
        die();
    }
    
    // if ($street == "") {
    //     echo "Please enter the user's street.";
    //     die();
    // }
    //
    // if ($city == "") {
    //     echo "Please enter the user's city.";
    //     die();
    // }

	if ($st == "")
		$st = "AL";
	
    if ($member_type == "") {
        echo "Please enter the user's member_type.";
        die();
    }

    if ($last_renewal_date == "") {
        echo "Please enter the user's last renewal date.";
        die();
    }

    if ($expiration_date == "") {
        echo "Please enter the user's expiration date.";
        die();
    }

    if (invalid_date($last_renewal_date)) {
        echo "The last renewal date is invalid.";
        die();
    }

    if (invalid_date($expiration_date)) {
        echo "The expiration date is invalid.";
        die();
    }

    // now that the dates have been confirmed, convert them from strings to date format
    $expiration_date = strtotime($expiration_date);
    $last_renewal_date = strtotime($last_renewal_date);

    $today = strtotime(date('Y/m/d H:i:s'));
    
    $lrd_year = date('Y', strtotime($last_renewal_date));
    if ($last_renewal_date > $today) {
        echo "Please check your last renewal date entry. It cannot be later than today's date.";
        die();
    }

    $exp_year = date('Y', $expiration_date);
    if ($exp_year < 2014 || $exp_year > 2030) {
        echo "Please check your expiration renewal date entry. The year doesn't make sense.";
        die();
    }

    // convert the dates (now in milliseconds) into a 'YYYY-mm-dd' format
    $expiration_date = date('Y-m-d', $expiration_date);
    $last_renewal_date = date('Y-m-d', $last_renewal_date);

    $created_at = date('Y-m-d H:i:s');
    $updated_at = date('Y-m-d H:i:s');
	
	// mark this user's account as being managed by apple directly
	$subscription_source = 'not_subscribed';
    
    $sql = "INSERT INTO users 
            (user_name, first_name, last_name, street, city, st,
            member_type, last_renewal_date, expiration_date, created_at, updated_at, subscription_source)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'ssssssssssss', 
            $user_name, $first_name, $last_name, $street, $city, $st,
            $member_type, $last_renewal_date, $expiration_date, $created_at, $updated_at, $subscription_source);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting user");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);
    
    $_SESSION['session_time'] = time();

    echo "success";

?>
