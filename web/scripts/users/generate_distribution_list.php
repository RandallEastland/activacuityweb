<?php

    session_start();
	
    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    // GROUP BY ensures only one email per registered email address
    $sql = "SELECT 		email
			FROM 		users
			GROUP BY 	(email)";
			
    $users = mysqli_query($mysqli, $sql);

	$distribution_list = "";
	$count = 0;
	
    while ($user = mysqli_fetch_array($users)) {

		if ($count > 0)
			$distribution_list .= ", ";
		
        $distribution_list .= $user['email'];
		
		$count++;
    }

    mysqli_close($mysqli);

	echo $distribution_list;
	
?>
