<?php

session_start();

$path = $_SERVER['DOCUMENT_ROOT'];
$connection = $path . '/scripts/connection.php';
include ($connection);

// GROUP BY ensures only one email per registered email address
$sql = "SELECT 		last_name, first_name, email, created_at, expiration_date, member_type, user_name, last_renewal_date
		FROM 		users
		ORDER BY 	last_name, first_name";

$users = mysqli_query($mysqli, $sql);

header("Content-Type: text/csv");
header('Content-Disposition: attachment; filename=activacuity%20users.csv');

$array = array();
$out = fopen('php://output', 'w');
while ($user = mysqli_fetch_array($users)) {
	fputcsv($out, $user);
}
fclose($out);
?>
