<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    $date_string_validator = $path . '/scripts/date_string_validator.php';
    include ($connection);
    include ($date_string_validator);

    //for passing confirmation messages
    //session_start();
     
    //retrieve input values from the form
    $user_id            =$_POST['user_id'];
    $user_name          = strip_tags($_POST['user_name']);
    $first_name         = strip_tags($_POST['first_name']);
    $last_name          = strip_tags($_POST['last_name']);
    $street             = strip_tags($_POST['street']);
    $city               = strip_tags($_POST['city']);
    $st                 = strip_tags($_POST['st']);
    $member_type        = strip_tags($_POST['member_type']);
	$race_id			= strip_tags($_POST['race_id']);
    $last_renewal_date  = strip_tags($_POST['last_renewal_date']);
    $expiration_date    = strip_tags($_POST['expiration_date']);
	$include_in_progress_report = strip_tags($_POST['include_in_progress_report']);
	$report_section		= strip_tags($_POST['report_section']);
    
    //make sure all fields have been entered
    if ($user_name == "") {
        echo "Please enter the username.";
        die();
    }
        
    if ($first_name == "") {
        echo "Please enter the first name of this user.";
        die();
    }
    
    if ($last_name == "") {
        echo "Please enter the last name of this user.";
        die();
    }
    
	if ($st == "") $st = "AL";
	
    if ($member_type == "") {
        echo "Please enter the user's member_type.";
        die();
    }

    if ($last_renewal_date == "") {
        echo "Please enter the user's last renewal date.";
        die();
    }

    if ($expiration_date == "") {
        echo "Please enter the user's expiration date.";
        die();
    }

    if (invalid_date($last_renewal_date)) {
        echo "The last renewal date is invalid.";
        die();
    }

    if (invalid_date($expiration_date)) {
        echo "The expiration date is invalid.";
        die();
    }
	
	$include_in_progress_report = $include_in_progress_report == "1" ? 1 : 0;
		
    // now that the dates have been confirmed, convert them from strings to date format
    $expiration_date = strtotime($expiration_date);
    $last_renewal_date = strtotime($last_renewal_date);

    $today = strtotime(date('Y/m/d H:i:s'));
    
    $lrd_year = date('Y', strtotime($last_renewal_date));
    if ($last_renewal_date > $today) {
        echo "Please check your last renewal date entry. It cannot be later than today's date.";
        die();
    }

    $exp_year = date('Y', $expiration_date);
    if ($exp_year < 2017 || $exp_year > 2099) {
        echo "Please check your expiration renewal date entry. The year doesn't make sense.";
        die();
    }

    // convert the dates (now in milliseconds) into a 'YYYY-mm-dd' format
    $expiration_date = date('Y-m-d', $expiration_date);
    $last_renewal_date = date('Y-m-d', $last_renewal_date);

    $updated_at = date('Y-m-d H:i:s');
 
    $sql = "UPDATE users 
            SET user_name=?, 
                first_name=?, 
                last_name=?, 
                street=?, 
                city=?, 
                st=?, 
                member_type=?,
				race_id=?,
                last_renewal_date=?, 
                expiration_date=?,
                updated_at=?,
				include_in_progress_report=?,
				report_section=?
            WHERE user_id=?";
    $stmt = mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'sssssssisssisi', 
            $user_name, $first_name, $last_name, $street, $city, $st, $member_type, $race_id, $last_renewal_date, $expiration_date, $updated_at, $include_in_progress_report, $report_section, $user_id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);

    //new user created, refresh the session time variable
    $_SESSION['session_time'] = time();

    echo "success";
?>
