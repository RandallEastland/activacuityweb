<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //retrieve input values from the form
    $segment_id     = $_POST['segment_id'];
    $program_number = strip_tags($_POST['program_number']);
    $description    = strip_tags($_POST['description']);
    
    //make sure all fields have been entered
    if ($segment_id == "") {
        echo "Please enter the segment that this program belongs to.";
        die();
    }
    
    if ($program_number == "") {
        echo "Please enter the program number.";
        die();
    }
        
    if ($description == "") {
        echo "Please enter a description to display on the app.";
        die();
    }
    
    if (!is_numeric($program_number)) {
        echo "Please enter a numeric value for the program number.";
        die();
    }

    $sql = "INSERT INTO programs (segment_id, prog_num, description)
            VALUES (?,?,?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'iis', 
            $segment_id, $program_number, $description);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting record");
        }
        
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);
    
    $_SESSION['session_time'] = time();

    echo "success";

?>
