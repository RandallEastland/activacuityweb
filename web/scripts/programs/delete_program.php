<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    session_start();
     
    //retrieve input values from the form
    $id = $_POST['id'];

    //check to see if this program has any child sessions
    $sql = "SELECT session_id 
            FROM sessions 
            WHERE prog_id = $id";
    $result = mysqli_query($mysqli, $sql);
    if (!$result) {echo "dependency query failed";die();}
    $count = mysqli_num_rows($result);
    if ($count > 0) {
        echo "dependency|".$count;
        die();
    }

    $sql = "DELETE FROM programs WHERE prog_id='$id'";
    $result = mysqli_query($mysqli, $sql);

    if (!$result) {
        echo "Error removing program";
        die();
    }

    //coach successfully deleted, update the session time variable
    $_SESSION['session_time'] = time();

    mysqli_close($mysqli);

    echo "success";

?>
