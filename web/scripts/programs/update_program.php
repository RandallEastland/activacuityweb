<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    //session_start();
     
    //retrieve input values from the form
    $id             = $_POST['id'];
    $segment_id     = $_POST['segment_id'];
    $program_number = strip_tags($_POST['program_number']);
    $description    = strip_tags($_POST['description']);

    $sql = "UPDATE programs 
            SET segment_id=?, 
                prog_num=?, 
                description=?
            WHERE prog_id=?";
    $stmt = mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'sisi', 
            $segment_id, $program_number, $description, $id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);

    //new user created, refresh the session time variable
    $_SESSION['session_time'] = time();

    echo "success";
    //echo $segment_id."|". $program_number."|". $description;

?>
