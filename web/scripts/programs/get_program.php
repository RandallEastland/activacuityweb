<?php

    include('../connection.php');

    $id = $_POST['id'];
   
    //get information on this coach from the database
    $sql = "SELECT * FROM programs WHERE prog_id=$id";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        echo "failure";
        die();
    }

    $program = mysqli_fetch_array($result);
    
    $segment_id     = $program['segment_id'];
    $program_number = $program['prog_num'];
    $description    = $program['description'];

    mysqli_close($mysqli);

    echo "$segment_id|$program_number|$description";

?>
