<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    session_start();
     
    //retrieve input values from the form
    $id = $_POST['id'];

    $sql = "DELETE FROM sessions WHERE session_id='$id'";
    $result = mysqli_query($mysqli, $sql);

    if (!$result) {
        echo "Error removing session";
        die();
    }

    //coach successfully deleted, update the session time variable
    $_SESSION['session_time'] = time();

    mysqli_close($mysqli);

    echo "success";

?>
