<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //retrieve input values from the form
    $prog_id      = strip_tags($_POST['prog_id']);
    $session_num  = strip_tags($_POST['session_num']);
    $media_file   = strip_tags($_POST['media_file']);
    $seconds      = strip_tags($_POST['time_seconds']);
    
    //make sure all fields have been entered
    if ($session_num == "") {
        echo "Please enter the session number.";
        die();
    }
        
    if (!is_numeric($session_num)) {
        echo "Please enter a numeric value for the session number.";
        die();
    }
    
    if ($media_file == "") {
        echo "Please enter the name of the media file.";
        die();
    }

    if ($seconds == "") {
        echo "Please enter the number of seconds that this audio file plays.";
        die();
    }

    $sql = "INSERT INTO sessions (prog_id, session_num, media_file, play_seconds)
            VALUES (?,?,?,?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'iisi', 
            $prog_id, $session_num, $media_file, $seconds);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting record");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);
    
    $_SESSION['session_time'] = time();

    echo "success";

?>
