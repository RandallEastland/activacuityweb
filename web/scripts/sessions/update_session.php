<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    //session_start();
     
    //retrieve input values from the form
    $session_id  = $_POST['session_id'];
    $prog_id     = $_POST['prog_id'];
    $session_num = strip_tags($_POST['session_num']);
    $media_file  = strip_tags($_POST['media_file']);
    $seconds     = strip_tags($_POST['time_seconds']);

    if ($session_num == "") {
        echo "Please enter a session number.";
        die();
    }
        
    if (!is_numeric($session_num)) {
        echo "Please enter a numeric value for the session number.";
        die();
    }
    
    if ($media_file == "") {
        echo "Please enter the media filename.";
        die();
    }

    if ($seconds == "") {
        echo "Please enter the number of seconds that this audio file plays.";
        die();
    }

    $sql = "UPDATE sessions 
            SET session_num=?, 
                prog_id=?,
                media_file=?,
                play_seconds=?
            WHERE session_id=?";
    $stmt = mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'iisii', 
            $session_num, $prog_id, $media_file, $seconds, $session_id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);

    //new user created, refresh the session time variable
    $_SESSION['session_time'] = time();

    echo "success";
    //echo $session_id."|". $prog_id."|". $session_num."|". $media_file;

?>
