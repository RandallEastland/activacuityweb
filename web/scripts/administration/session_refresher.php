<?php

    //This script resets the 'session_time' variable. It should be called every time 
    //the user initiates any action

    //start the php session (required for each page that accesses session variables)
    session_start();

    $_SESSION['session_time'] = time();

    echo "success";
 
?>
