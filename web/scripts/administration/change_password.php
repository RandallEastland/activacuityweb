<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    session_start();

    //make sure that there haven't been too many attempts
    if ($_SESSION['password_change_attempts'] > $_SESSION['max_attempts']) {
        echo "Too many attempts.";
        session_destroy();
        die();
    }
    
    //retrieve input values from the form
    $user_name         = strip_tags($_POST['user_name']);
    $current_plaintext = strip_tags($_POST['current_plaintext']);
    $new_plaintext     = strip_tags($_POST['new_plaintext']);
    $confirm_plaintext = strip_tags($_POST['confirm_plaintext']);
    
    if ($user_name == "") {
        echo "Please enter your user name.";
        die();
    }

    if ($current_plaintext == "") {
        echo "Please enter your current password.";
        die();
    }

    if ($new_plaintext == "") {
        echo "Please enter a new password.";
        die();
    }

    if ($confirm_plaintext == "") {
        echo "Please confirm your new password.";
        die();
    }

    if ($new_plaintext != $confirm_plaintext) {
        echo "Your password entries do not match.";
        die();
    }

    //strip out whitespace
    $user_name = preg_replace('/\s+/', '', $user_name);
    $plaintext = preg_replace('/\s+/', '', $new_plaintext);

    //verify that the current passwod matches the database value 
    $sql = "SELECT hashed_password FROM administrators WHERE user_name='$user_name'";
    $result = mysqli_query($mysqli, $sql);
    
    if (!$result) {
        mysqli_close($mysqli);
        echo "Error accessing database";
        die();
    }

    if (mysqli_num_rows($result) == 0) {
        echo "no matching user found.";
        die();
    }

    $row = mysqli_fetch_array($result);
    $db_password = $row['hashed_password'];

    // verify the current password entry
    $hashed_password_entry = crypt($current_plaintext, $db_password);
    if ($hashed_password_entry != $db_password) {
        echo "Invalid password entered.";
        die();
    }

    //hash the new password
    $salt = uniqid(); // Could use the second parameter to give it more entropy.
    $algo = '6'; // This is CRYPT_SHA512 as shown on http://php.net/crypt
    $rounds = '5000'; // The more, the more secure it is!
    $crypt_salt = '$' . $algo . '$rounds=' . $rounds . '$' . $salt; // This is the "salt" string we give to crypt().
    $hashed_password = crypt($plaintext, $crypt_salt);
        
    $sql = "UPDATE administrators
            SET hashed_password='$hashed_password' 
            WHERE user_name='$user_name'";
    $result = mysqli_query($mysqli, $sql);

    if (!$result) {
        mysqli_close($mysqli);
        echo "Error updating password: " . mysqli_error(); 
        die();
    }

    mysqli_close($mysqli);
    
    //Password updated - return success indicator
    echo "success";
    
?>
