<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //for passing confirmation messages
    session_start();
    $key = rand(1, 1000);
    //$next = "http://mobilenicity.com/RevRunning/new_admin_feedback.html?message=$key";
    
    //retrieve input values from the form
    $security_code  = strip_tags($_POST['security_code']);
    $user_name      = strip_tags($_POST['user_name']);
    $pass1          = strip_tags($_POST['plaintext1']);
    $pass2          = strip_tags($_POST['plaintext2']);

    //strip out any whitespace
    $username = preg_replace('/\s+/', '', $username);
    $plaintext = preg_replace('/\s+/', '', $pass1);

    //make sure all fields have been entered
    if (strlen($security_code) == 0) {echo "Please enter a security code."; die();}
    if ($user_name == "") {echo("Please enter a username."); die();}
    if ($pass1 == "") {echo "Please enter a password."; die();}
    if ($pass2 == "") {echo "Please enter a password confirmation."; die();}
    if ($pass1 != $pass2) {echo "Your password entries do not match."; die();} 
    
    //get the security code from the database
    $sql = "SELECT security_code FROM admin_code WHERE id = '1'";
    $result = mysqli_query($mysqli, $sql);
    if (!$result) {
        mysqli_close($mysqli);
        echo "Error querying database";
        die();
    }
    
    $row = mysqli_fetch_array($result);
    $db_security_code = $row['security_code'];
    
    if (!$db_security_code) {
        mysqli_close($mysqli);
        echo "Error retrieving security code from database.";
        die();
    }

//    if (crypt($security_code, $db_security_code) != $db_security_code) {
    if ($security_code != $db_security_code) {
        echo "Invalid security code.";
        die();
    }
    //make sure that username is unique
    $sql = "SELECT user_name FROM administrators WHERE user_name = '$user_name'";
    $result = mysqli_query($mysqli, $sql);
    if (mysqli_num_rows($result) > 0) {
        mysqli_close($mysqli);
        echo "This username already exists. Please choose a different username.";
        die();
    }
    
    //hash the password
    // These only work for CRYPT_SHA512, but it should give you an idea of how crypt() works.
    $salt = uniqid(); // Could use the second parameter to give it more entropy.
    $algo = '6'; // This is CRYPT_SHA512 as shown on http://php.net/crypt
    $rounds = '5000'; // The more, the more secure it is!
    $crypt_salt = '$' . $algo . '$rounds=' . $rounds . '$' . $salt; // This is the "salt" string we give to crypt().
    $hashed_password = crypt($plaintext, $crypt_salt);
    $session_id = 0;
    
    $sql = "INSERT INTO administrators (user_name, hashed_password, session_id)
        VALUES (?, ?, ?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'sss', 
            $user_name, $hashed_password, $session_id);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting new administrator record");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);

    $_SESSION['session_time'] = time();
 
    //new user created
    echo "success";
    
?>
