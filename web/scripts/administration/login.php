<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    session_start();

    //retrieve input values from the form
    $username  = strip_tags($_POST['username']);
    $plaintext = strip_tags($_POST['plaintext']);

    if ($username == "") {
        echo "Please enter a username";
        die();
    }
    
    if ($plaintext == "") {
        echo "Please enter a password";
        die();
    }
        
    //strip out whitespace
    $username = preg_replace('/\s+/', '', $username);
    $plaintext = preg_replace('/\s+/', '', $plaintext);

    $stmt =  mysqli_stmt_init($mysqli);
    $sql = "SELECT hashed_password
            FROM administrators
            WHERE user_name=?
            LIMIT 1";
    $result;
    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* bind parameters for markers */
        mysqli_stmt_bind_param($stmt, "s", $username);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (!stmt) {
            mysqli_close($link);
            echo "Query Error";
            die();
        }
        
        /* Bind results to variable */
        mysqli_stmt_bind_result($stmt, $db_password);

        // Fetch Value
        mysqli_stmt_fetch($stmt);

        /* close statement */
        mysqli_stmt_close($stmt);
    }
    
    if (!$db_password) {
        echo "Error retrieving password from database.";
        mysqli_close($mysqli);
        die();
    }

    // hash the password entry and make sure it matches what
    // is stored in the database
    if (crypt($plaintext, $db_password) != $db_password) {
        mysqli_close($mysqli);
        echo "Invalid Login Credentials";
        $_SESSION['login_attempts']++;
        die();
    }

    ////////////////////////////////////////////////////////////////
    // if we get here, the user's credentials have been validated
    // proceed by setting session variables

    session_start();

    //login attempt variable initializations
    $_SESSION['login_attempts'] = 0;
    $_SESSION['password_change_attempts'] = 0;
    $_SESSION['max_attempts'] = 5;

    //store the session id in the db for later authentication
    $session_id = session_id();
    $sql = "UPDATE administrators
            SET session_id='$session_id'
            WHERE user_name='$username'";
    $result = mysqli_query($mysqli, $sql);
    if (!$result) {
        $current .= "Error starting session";
        //file_put_contents($file, $current);
        session_destroy();
        mysqli_close($mysqli);
        echo "Error starting session";
        die();
    }

    //store the username in a session variable for later authentication
    $_SESSION['activeuser'] = $username;
    
    //store the time of the session for inactivity time out purposes later
    $_SESSION['session_time'] = time();
    $_SESSION['max_inactivity_period'] = 30 * 60; //30 minutes x 60 seconds/minute
    $_SESSION['logged_in'] = true; // boolean

    mysqli_close($mysqli);
    
    //return success indicator
    echo "success";
    
?>
