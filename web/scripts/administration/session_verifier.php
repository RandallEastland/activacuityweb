<?php

function session_verified() {

    //This script verifies that there is an active session for this user
    //start the php session (required for each page that accesses session variables)
    session_start();

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //confirm that an active session exists
    if(isset($_SESSION['logged_in'])) {
        
        //check if maximum inactivity period has been reached (set in 'login.php')
        $currenttime = time();
        $last_activity = $_SESSION['session_time'];
        $max_inactivity_period = $_SESSION['max_inactivity_period'];
        $this_inactivity_period = $currenttime - $last_activity;

        if ($this_inactivity_period >= $max_inactivity_period){
            mysqli_close($mysqli);
            session_destroy();
            return 0;
        }
         
    } else { // no active session
        mysqli_close($mysqli);
        session_destroy();
        return 0;
    }
 
    mysqli_close($mysqli);

    //Session verified - refresh the session time variable
    $_SESSION['session_time'] = time();
 
    //echo "verified";
    return 1;
}
?>
