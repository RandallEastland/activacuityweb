<?php

    include('../connection.php');

    $race_id = strip_tags($_POST['race_id']);
    
    //get information on this coach from the database
    $sql = "SELECT * FROM races WHERE race_id=$race_id";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        echo json_encode(array('result'=>'failure'));
        die();
    }

    $race = mysqli_fetch_array($result);

	$array = array(
		'race_id'		  => $race['race_id'],
		'race_name'		  => $race['race_name'],
		'woo_commerce_id' => $race['woo_commerce_id'],
		'race_date'		  => $race['race_date'],
		'expiration_text' => $race['expiration_text']
	);
	
    mysqli_close($mysqli);
	
    echo json_encode(array('result'=>'success', 'data'=>$array));
?>
