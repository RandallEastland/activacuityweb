<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //retrieve input values from the form
    $race_name   	 = strip_tags($_POST['race_name']);
    $woo_commerce_id = strip_tags($_POST['woo_commerce_id']);
    $race_date 		 = strip_tags($_POST['race_date']);
	$expiration_text = strip_tags($_POST['expiration_text']);
	
    //make sure all fields have been entered
    if ($race_name == "") {
        echo "Please enter a name for this race.";
        die();
    }
        
    if ($woo_commerce_id == "") {
        echo "Please tell me how to connect this race to Woo Commerce.";
        die();
    }
    
    if ($race_date == "") {
        echo "Please tell me when this race occurs.";
        die();
    }
	
	if ($expiration_text == "") {
		echo "Please tell me what you want the user to see after the race is over.";
		die();
	}

    $sql = "INSERT INTO races (race_name, woo_commerce_id, race_date, expiration_text)
            VALUES (?,?,?,?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'ssss', $race_name, $woo_commerce_id, $race_date, $expiration_text);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting record");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);
    }
    
    mysqli_close($mysqli);
    
    $_SESSION['session_time'] = time();

    echo "success";

?>
