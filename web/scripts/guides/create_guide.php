<?php

    $path = $_SERVER['DOCUMENT_ROOT'];
    $connection = $path . '/scripts/connection.php';
    include ($connection);

    //retrieve input values from the form
    $guide_num   = strip_tags($_POST['guide_num']);
    $description = strip_tags($_POST['description']);
    $parent_id   = strip_tags($_POST['parent_id']);
    $elaboration = $_POST['elaboration']; // don't strip the tags since this field allows formatting
    
    //make sure all fields have been entered
    if ($guide_num == "") {
        echo "Please enter the guide number so that the app can sort this in the correct order.";
        die();
    }

    if ($guide_num == 0) {
        echo "The guide number cannot be 0. This is reserved for the top level parent list only.";
        die();
    }

    if ($guide_num == $parent_id) {
        echo "The guide number cannot match the parent number.";
        die();
    }
    
    if (!is_numeric($guide_num)) {
        echo "Please enter a numeric value for the guide number.";
        die();
    }
    
    if ($description == "") {
        echo "Please enter a description (question) for this guide. This appears on the main list in the app.";
        die();
    }
    
    if ($parent_id == "") {
        echo "Please enter the parent list number. Enter 0 if this item appears on the main list.";
        die();
    }

    if ($elaboration == "") {
        echo "Please elaborate on this item. This is where you give the user information on the question that they ask.";
        die();
    }

    //////////////////////////////////////////////////////////////////////////////////
    // make sure that this guide number is unique
    $sql = "SELECT guide_num FROM guides WHERE guide_num = ?";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'i', $guide_num);

        /* execute query */
        mysqli_stmt_execute($stmt);

        /* store result */
        mysqli_stmt_store_result($stmt);

        if (mysqli_stmt_num_rows($stmt) > 0) {
            mysqli_stmt_close($stmt);
            die("This guide number is already in use. Please select a unique guide number.");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);

    }

    //////////////////////////////////////////////////////////////////////////////////
    // everything is in order. Insert the new record.
    $sql = "INSERT INTO guides (guide_num, description, parent_id, elaboration)
            VALUES (?,?,?,?)";
    $stmt =  mysqli_stmt_init($mysqli);

    if (mysqli_stmt_prepare($stmt, $sql)) {

        /* Bind the input parameters to the query */
        mysqli_stmt_bind_param($stmt, 'isis', 
            $guide_num, $description, $parent_id, $elaboration);

        /* execute query */
        mysqli_stmt_execute($stmt);

        if (mysqli_stmt_affected_rows($stmt) != 1) {
            mysqli_stmt_close($stmt);
            die("error inserting record");
        }
         
        /* close statement */
        mysqli_stmt_close($stmt);

    }
    
    mysqli_close($mysqli);
    
    $_SESSION['session_time'] = time();

    echo "success";

?>
