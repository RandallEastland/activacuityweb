<?php

    include('../connection.php');

    $guide_id = $_POST['id'];
    
    //get information on this coach from the database
    $sql = "SELECT * FROM guides WHERE guide_id=$guide_id";
    $result = mysqli_query($mysqli, $sql);
    
    if(!$result) {
        echo "failure";
        die();
    }

    $program = mysqli_fetch_array($result);
    
    $guide_num   = $program['guide_num'];
    $description = $program['description'];
    $parent_id   = $program['parent_id'];
    $elaboration = $program['elaboration'];

    mysqli_close($mysqli);

    echo "$guide_num|$description|$parent_id|$elaboration";

?>
