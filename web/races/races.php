<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Races</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

        #bodyframe{
            margin:0 auto;
            width:800px;
            float:center;
            clear:left;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script language="javascript" type="text/javascript">

        function deleteThis(btn_id) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            var race_id = btn_id.split('-')[1];

            //confirm intention to delete
            var question = "Are you sure that you want to delete this race?";            
            if (!confirm(question)) {return;}
	        var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;

                    if (response == "success") {
                        window.location.reload();
                    }
                }
            }

            var content = "race_id=" + encodeURIComponent(race_id);
            
            xmlhttp.open("POST", "../scripts/races/delete_race.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
 
        function updateThis(id, list, program_number, description, media_url) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = id.split('-')[1];
            
            var next = "update_race.php?id="+id;
            
            window.location.href = next;
            
        }
        
        function addNewRace() {
            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            window.location.href = "new_race.php";
        }
    
    </script>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Races</h1>
            <p>Use this area to update the list of races.</p>
        </div>
    </div>
    
    <div id="bodyframe">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="addNewRace()">Add New Race</button>          
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>


        <table id="dataset">
            <tr class="dataheader" valign='bottom'>
                <td>Race Name</td>
                <td>WooCommerce Identifier</td>
				<td>Race Date</td>
                <td align='center'>+</td>
                <td align='center'>-</td>
            </tr>

            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $connection = $path . '/scripts/connection.php';
                include ($connection);

                $sql="SELECT * FROM races WHERE race_id>0 ORDER BY race_date DESC";
                $result = mysqli_query($mysqli, $sql);
                if (!$result) {die("SQL error retrieving race list.");}

                if (mysqli_num_rows($result) == 0) {
                    echo "
                        The list of races is empty.

                        ";
                    die();
                }
                    
                while ($row = mysqli_fetch_array($result)) {

                    $race_name  	 = $row['race_name'];
					$woo_commerce_id = $row['woo_commerce_id'];
					$race_date 		 = $row['race_date'];
                    
                    //make unique names for each <td> in this row
                    $race_id  = $row['race_id']; //this is the id for this segment in the database 
                    
                    $id_hdr            	 = "id_hdr-"            	. $race_id;
                    $race_name_hdr  	 = "race_name_hdr-"  		. $race_id;
                    $woo_commerce_id_hdr = "woo_commerce_id_hdr-" 	. $race_id;
					$race_date_hdr		 = "race_date_hdr-"			. $race_id;
                    $btn_update          = "btn_update-"        	. $race_id;
                    $btn_delete          = "btn_delete-"        	. $race_id;
                    
                    //Build this row of the table
                    $tableString = 
                        "<tr class='datarow'>"  .  
                            "<td class='id'         id='$id_hdr' style='display:none'>" . $race_id    		. "</td>" .
							"<td class='cc' 		id='$race_name_hdr'>"				. $race_name		. "</td>" .
                            "<td class='cc'         id='$woo_commerce_id_hdr'>"         . $woo_commerce_id  . "</td>" .
                            "<td class='c0'         id='$race_date_hdr'>"           	. $race_date 		. "</td>" .
                       
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/update.png' id='$btn_update' 
                                    onclick='updateThis(this.id)'>".
                            "</td>" .
                       
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/delete.png' id='$btn_delete' 
                                    onclick='deleteThis(this.id, \"$description\")'>".
                            "</td>" .
                        "</tr>";
                        
                    echo $tableString;
                }    
           
                mysqli_close($mysqli);
            ?>

        </table>

    </div>

</body>

</html>
