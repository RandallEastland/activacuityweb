<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Update Race</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script type="text/javascript">
   
        window.onload = function getRaceInfo() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var race_id = document.URL.split('=')[1];

            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = JSON.parse(xmlhttp.responseText);
                    // console.log(response);
                    if (response.result == "success") {

                        document.getElementById("race_id").value   		 = response.data.race_id;
						document.getElementById("race_name").value   	 = response.data.race_name;
						document.getElementById("woo_commerce_id").value = response.data.woo_commerce_id;
						document.getElementById("race_date").value   	 = response.data.race_date;
						document.getElementById("expiration_text").value = response.data.expiration_text;

                    } else {
                        document.getElementById("rightsidebox").innerHTML="Failed to retrieve race information.";
                    }
                }
            }

            var content = "race_id=" + encodeURIComponent(race_id);
            
            xmlhttp.open("POST", "../scripts/races/get_race.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);
            
            return false;
        }

        function updateRace() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
			var race_id			= document.getElementById("race_id").value;
            var race_name   	= document.getElementById("race_name").value;
            var woo_commerce_id = document.getElementById("woo_commerce_id").value;
            var race_date 		= document.getElementById("race_date").value;
			var expiration_text = document.getElementById("expiration_text").value;

            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response == "success") {
                        //alert("Changes to " + name + " updated successfully");
                        window.location.href = "races.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

			var content =   "race_id="			+encodeURIComponent(race_id)+
							"&race_name="    	+encodeURIComponent(race_name)+
			                "&woo_commerce_id=" +encodeURIComponent(woo_commerce_id)+
			                "&race_date=" 		+encodeURIComponent(race_date)+
							"&expiration_text=" +encodeURIComponent(expiration_text);
            
            xmlhttp.open("POST", "../scripts/races/update_race.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
        
        
        function goToDashboard() {
            window.location.href = "../dashboard.html";
        }
    
    </script>

    <div id="titleframe">

		<div id="race_id" style="display: none;"></div>
			
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Update Race</h1>
            <p>Make any changes that you wish - then click Update Race</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="updateRace()">Update Race</button>
            <button type="submit" class="bigbutton" onClick="parent.location='races.php'">All Races</button>
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>

        </div>
        
        <div id="additem" >
                
            <form id="form" name="form" method="post" action="../scripts/create_race.php">

                <label>Race Name
                    <span class="small">What is the name of this race?</span>
                </label>
                <input type="text" id="race_name" autofocus/>

                <label>Woo Commerce Identifier
                    <span class="small">This must match up with the value received with new subscribers.</span>
                </label>
                <input type="text" id="woo_commerce_id" />

                <label>Race Date
                    <span class="small">Date on which the race occurs.</span>
                </label>
				<input type="date" id="race_date" class="datePicker"/>

                <label>Expiration Notification Text
                    <span class="small">What do you want the app to display after the race has concluded?</span>
                </label>
				<textarea rows="8" cols="50" id="expiration_text">Thanks for using ActivAcuity to prepare for your race. We sincerely hope that it helped you achieve your goals.
					
Please consider subscribing again for your next race! 
	
~Terry Chiplin
				</textarea>
              
            </form>

        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
