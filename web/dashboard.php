<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Dashboard</title>
    <link rel="icon" type="image/png" href="images/logo.png" />
    <link rel="stylesheet" type="text/css" href="styles/mystyles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="styles/bigbuttons.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="styles/datapages.css" media="screen" />
    
    <style media="screen" type="text/css">

        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

    </style> 

    <script type="text/javascript" src="js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (!session_verified()) {
            // no active session found - kick the user back to the signin page 
            header('Location: signin.php');
        }
    ?>

</head>

<body>
    
    <div id="titleframe">
        <div class="logoimage">
            <img id="mainpic" src="images/logo.png">
        </div>

        <div class="titletext">
            <img class="title_image" id="mainpic" src="images/title.png">
            <h1>Dashboard</h1>
        </div>
    </div>

    <div width="630px">
        <table class="three_button_row">
            <tr>
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='segments/segments.php'">Segments</button>
                </td>
				
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='programs/programs.php'">Programs & Sessions</button>
                </td>
                    
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='races/races.php'">Races</button>
                </td>
                    
            </tr>
        </table>

        <table class="three_button_row">
            <tr>
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='users/users.php'">Users</button>
                </td>

                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='guides/guides.php'">Guides</button>
                </td>
                
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='administration/administration.php'">Administration</button>
                </td> 
                
            </tr>
        </table>
		
		<table class="one_button_row">
			<tr>
                <td>
                    <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
                </td> 
			</tr>
		</table>

    </div>  

</body>

</html>
