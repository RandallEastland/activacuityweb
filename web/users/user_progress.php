<?php
session_start();
// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Users</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:730px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:20px;
            width:450px;
        }

        #bodyframe{
            margin:0 auto;
            width:1070px;
            float:center;
            clear:left;
			font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
			font-size:10px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

</head>

<body>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>User Progress Report</h1>
        </div>
    </div>
    
    <div id="bodyframe">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
        </div>

        <table id="dataset">
            <tr class="dataheader">
                <td>Section</td>
                <td>Name</td>
				<td>Email Address</td>
				<td>User Name</td>
				<td>Segment</td>
				<td>Program</td>
				<td align='right'>Session</td>
				<td align='right'>Count</td>
            </tr>

            <?php

                $path = $_SERVER['DOCUMENT_ROOT'];
                $connection = $path . '/scripts/connection.php';
                include ($connection);

                $sql = "SELECT 	progress.user_name, 
								sessions.session_num AS session, 
								programs.description AS program, 
								segments.segment_name AS segment,
								users.first_name AS first_name,
								users.last_name AS last_name,
								users.email AS email,
								users.report_section as section,
								Count(*) as count
						FROM progress
						JOIN sessions 
							ON progress.session_id = sessions.session_id
						JOIN programs
							ON sessions.prog_id = programs.prog_id
						JOIN segments
							ON programs.segment_id = segments.segment_id
						JOIN users
							ON progress.user_name = users.user_name
						WHERE users.include_in_progress_report = 1
						GROUP BY progress.user_name, program, session
						ORDER BY section, last_name, first_name, segments.segment_id, programs.prog_id";

                $result=mysqli_query($mysqli, $sql);
                if (!$result) {die("SQL error retrieving users.");}

                if (mysqli_num_rows($result) == 0) {
                    echo "The list of users is empty.<br><br>";
                    die();
                }
                    
                while ($row = mysqli_fetch_array($result)) {

                    //make unique names for each <td> in this row
                    $user_id 			 = $row['user_id']; //this is the id for this athlete in the database                     
                    $name       		 = $row['last_name'] . ", " . $row['first_name'];

                    //Build this row of the table
                    $tableString = 
                        "<tr class='datarow'>"  .  
							"<td class='c0'>"    			. $row['section']  	. "</td>" .
                            "<td class='cc'>"    			. $name            	. "</td>" .
							"<td class='cc'>"    			. $row['email']		. "</td>" .
							"<td class='lx'>"    			. $row['user_name']	. "</td>" .
                            "<td class='cc'>"	 			. $row['segment']	. "</td>" .
							"<td class='c0'>"	 			. $row['program'] 	. "</td>" .
							"<td class='c0' align='right'>" . $row['session']  	. "</td>" .
							"<td class='c0' align='right'>" . $row['count'] 	. "</td>" .
                        "</tr>";
                        
                    echo $tableString;
                     
                }    
                
                mysqli_close($mysqli);
            ?>

        </table>
                       
    </div>

</body>

</html>
