<?php
session_start();
// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Users</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:730px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:20px;
            width:450px;
        }

        #bodyframe{
            margin:0 auto;
            width:1070px;
            float:center;
            clear:left;
			font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
			font-size:10px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>
	<script type="text/javascript" src="../js/export_csv.js"></script>

</head>

<body>

    <script language="javascript" type="text/javascript">

        function deleteThis(id, name) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            //var id = btn_id.split('-')[1];

            //confirm intention to delete
            var question = "Are you sure that you want to delete " + name + "?";            
            if (!confirm(question)) {return;}
	        var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    //console.log(response);
                    if (response == "success") {
                        window.location.reload();
                    }
                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/users/delete_user.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
 
        function updateThis(id) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var next = "update_user.php?id="+id;
            
            window.location.href = next;
            
        }
        
        function addNewUser() {
            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            window.location.href = "new_user.php";
        }
    
    </script>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Users</h1>
            <p>Generally, users will maintain their own accounts through the app. Use this screen to make any changes you feel are necessary.</p>
        </div>
    </div>
    
    <div id="bodyframe">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="addNewUser()">Add New User</button>  
			<button type="submit" class="bigbutton" onClick="parent.location='distribution_list.php'">Email Distribution List</button>          
			<button type="submit" class="bigbutton" onClick="exportCSV()">Export to CSV</button>
			<button type="submit" class="bigbutton" onClick="parent.location='user_progress.php'">User Progress Report</button>
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>

        <table id="dataset">
            <tr class="dataheader">
                <td>Name</td>
				<td>User Name</td>
				<td>Email Address</td>
				<td>Member Type</td>
				<td>Subscription Source</td>
				<td>Race Name</td>
				<td>Last Renewal Date</td>
				<td>Expiration Date</td>
            </tr>

            <?php

                $path = $_SERVER['DOCUMENT_ROOT'];
                $connection = $path . '/scripts/connection.php';
                include ($connection);

                $sql = "SELECT 
							user_id, last_name, first_name, email, subscription_source, races.race_name, 
							user_name, member_type, last_renewal_date, expiration_date 
						FROM users 
						JOIN races ON users.race_id = races.race_id
						ORDER BY last_name, first_name, st, city";
                $result=mysqli_query($mysqli, $sql);
                if (!$result) {die("SQL error retrieving users.");}

                if (mysqli_num_rows($result) == 0) {
                    echo "The list of users is empty.<br><br>";
                    die();
                }
                    
                while ($row = mysqli_fetch_array($result)) {

                    //make unique names for each <td> in this row
                    $user_id 			 = $row['user_id']; //this is the id for this athlete in the database 
                    
                    $name       		 = $row['last_name'] . ", " . $row['first_name'];
					$user_name			 = $row['user_name'];
                    $firstlast  		 = $row['first_name'] . " " . $row['last_name'];
					$email				 = $row['email'];
					$subscription_source = $row['subscription_source'];
					$race_name			 = $row['race_name'];
					
					if ($race_name == 'All Races')
						$race_name = 'ActivAcuity';
                    
                    $id_hdr                 	= "id_hdr-"     		 . $user_id;
                    $name_hdr               	= "name_hdr-"   		 . $user_id;
					$user_name_hdr				= 'user_name_hdr-'		 . $user_id;
					$email_hdr					= "emai_hdr-"			 . $user_id;
                    $member_type_hdr        	= "member_type-"		 . $user_id;
					$subscription_source_hdr	= "subscription_source-" . $user_id;
					$race_name_hdr				= "race_name-"			 . $user_id;
                    $last_renewal_date_hdr  	= "lrd-"        		 . $user_id;
                    $expiration_date_hdr    	= "exp-"        		 . $user_id;
					
                    $btn_update             	= "btn_update-" 		 . $user_id;
                    $btn_delete             	= "btn_delete-" 		 . $user_id;

                    //Build this row of the table
                    $tableString = 
                        "<tr class='datarow'>"  .  
                            "<td class='id'    id='$id_hdr' style='display:none'>"  . $row['id']                . "</td>" .
                            "<td class='cc'    id='$name_hdr'>"                     . $name                     . "</td>" .
							"<td class='lx'    id='$user_namename_hdr'>"            . $user_name                . "</td>" .
							"<td class='cc;	   id='$email_hdr'>"					. $email					. "</td>" .
                            "<td class='c0'    id='$member_type_hdr_hdr'>"          . $row['member_type']       . "</td>" .
							"<td class='c0'	   id='$subscription_source_hdr'>"		. $subscription_source 		. "</td>" .
							"<td class='cc'	   id='$race_name_hdr'>"				. $race_name				. "</td>" .
                            "<td class='c0'    id='$last_renewal_date_hdr'>"        . $row['last_renewal_date'] . "</td>" .
                            "<td class='c0'    id='$exp_date_hdr'>"                 . $row['expiration_date']   . "</td>" .
                       
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/update.png' id='$btn_update' 
                                    onclick='updateThis(\"$user_id\")'>".
                            "</td>" .
                       
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/delete.png' id='$btn_delete' 
                                    onclick='deleteThis(\"$user_id\", \"$firstlast\")'>".
                            "</td>" .
                        "</tr>";
                        
                    echo $tableString;
                     
                }    
                
                mysqli_close($mysqli);
            ?>

        </table>
                       
    </div>

</body>

</html>
