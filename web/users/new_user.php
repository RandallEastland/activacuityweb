<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>New User</title>
    <link rel="icon" type="image/png" href="images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:730px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:450px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script type="text/javascript">

        function createUser() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
             
            var user_name           = document.getElementById("user_name").value;
            var first_name          = document.getElementById("first_name").value;
            var last_name           = document.getElementById("last_name").value;
            var street              = document.getElementById("street").value;
            var city                = document.getElementById("city").value;
//            var st                  = document.getElementById("st").value;
//            var member_type         = document.getElementById("member_type").value;
            var last_renewal_date   = document.getElementById("last_renewal_date").value;
            var expiration_date     = document.getElementById("expiration_date").value;

            var e = document.getElementById("st");
            var st = e.options[e.selectedIndex].value;

            var e = document.getElementById("member_type");
            var member_type = e.options[e.selectedIndex].value.toLowerCase();
            
            var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response == "success") {
                        var successResponse =  "User successfully added.";
                        document.getElementById("rightsidebox").innerHTML=successResponse;

                        //clear the text fields for next input
                        document.getElementById("user_name").value = "";
                        document.getElementById("first_name").value = "";
                        document.getElementById("last_name").value = "";
                        document.getElementById("street").value = "";
                        document.getElementById("city").value = "";
//                        document.getElementById("st").value = "";
//                        document.getElementById("member_type").value = "";
                        document.getElementById("last_renewal_date").value = "";
                        document.getElementById("expiration_date").value = "";

                        document.getElementById("user_name").focus();
                       
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "user_name="            +encodeURIComponent(user_name)+
                            "&first_name="          +encodeURIComponent(first_name)+
                            "&last_name="           +encodeURIComponent(last_name)+
                            "&street="              +encodeURIComponent(street)+
                            "&city="                +encodeURIComponent(city)+
                            "&st="                  +encodeURIComponent(st)+
                            "&member_type="         +encodeURIComponent(member_type)+
                            "&last_renewal_date="   +encodeURIComponent(last_renewal_date)+
                            "&expiration_date="     +encodeURIComponent(expiration_date);
                
            xmlhttp.open("POST", "../scripts/users/create_user.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
    
        function goToDashboard() {
            window.location.href = "../dashboard.php";
        }
    
    </script>

    <div id="titleframe">

        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>New User</h1>
            <p>Generally, users will maintain their own accounts through the app. Use this screen to make any changes you feel are necessary.</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="createUser()">Add User</button>
            <button type="submit" class="bigbutton" onClick="parent.location='users.php'">All Users</button>
            <button type="submit" class="bigbutton" onClick="goToDashboard()">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>
        
        <div id="additem" >
                
            <form id="form" name="form" method="post" action="../scripts/segments/create_segment.php">

                <label>User Name
                    <span class="small"></span>
                </label>
                <input type="text" id="user_name" autofocus/>
            
                <label>First Name
                    <span class="small"></span>
                </label>
                <input type="text" id="first_name" />
    
                <label>Last Name
                    <span class="small"></span>
                </label>
                <input type="text" id="last_name" />

                <label>Street Address
                    <span class="small"></span>
                </label>
                <input type="text" id="street" />

                <label>City
                    <span class="small"></span>
                </label>
                <input type="text" id="city" />

<!--                <label>State
                    <span class="small"></span>
                </label>
                <input type="text" id="st" />

                <label>Member Type
                    <span class="small">Paid vs Introductory</span>
                </label>
                <input type="text" id="member_type" />
-->

                <label>State
                    <span class="small"></span>
                </label>
                <select class="combobox" id="st">
                    <option value="AL">AL</option>
                    <option value="AK">AK</option>
                    <option value="AZ">AZ</option>
                    <option value="AR">AR</option>
                    <option value="CA">CA</option>
                    <option value="CO" selected="selected">CO</option>
                    <option value="CT">CT</option>
                    <option value="DE">DE</option>
                    <option value="DC">DC</option>
                    <option value="FL">FL</option>
                    <option value="GA">GA</option>
                    <option value="HI">HI</option>
                    <option value="ID">ID</option>
                    <option value="IL">IL</option>
                    <option value="IN">IN</option>
                    <option value="IA">IA</option>
                    <option value="KS">KS</option>
                    <option value="KY">KY</option>
                    <option value="LA">LA</option>
                    <option value="ME">ME</option>
                    <option value="MD">MD</option>
                    <option value="MA">MA</option>
                    <option value="MI">MI</option>
                    <option value="MN">MN</option>
                    <option value="MS">MS</option>
                    <option value="MO">MO</option>
                    <option value="MT">MT</option>
                    <option value="NE">NE</option>
                    <option value="NV">NV</option>
                    <option value="NH">NH</option>
                    <option value="NJ">NJ</option>
                    <option value="NM">NM</option>
                    <option value="NY">NY</option>
                    <option value="NC">NC</option>
                    <option value="ND">ND</option>
                    <option value="OH">OH</option>
                    <option value="OK">OK</option>
                    <option value="OR">OR</option>
                    <option value="PA">PA</option>
                    <option value="RI">RI</option>
                    <option value="SC">SC</option>
                    <option value="SD">SD</option>
                    <option value="TN">TN</option>
                    <option value="TX">TX</option>
                    <option value="UT">UT</option>
                    <option value="VT">VT</option>
                    <option value="VA">VA</option>
                    <option value="WA">WA</option>
                    <option value="WV">WV</option>
                    <option value="WI">WI</option>
                    <option value="WY">WY</option>
                </select>

                <label>Member Type
                    <span class="small">Paid vs Introductory</span>
                </label>
                <select class="combobox" id="member_type">
                    <option value="paid">Paid</option>
                    <option value="introductory">Introductory</option>
                </select>

                <label>Renewal Date
                    <span class="small">Last time the user paid for the app.</span>
                </label>
                <input type="date" id="last_renewal_date" class="datepicker">

                <label>Expiration Date
                    <span class="small">Scheduled date that membership will expire.</span>
                </label>
                <input type="date" id="expiration_date" class="datepicker">

<!--
                <label>Last Renewal Date
                    <span class="small">Last time the user paid for the app.</span>
                </label>
                <input type="text" id="last_renewal_date" />

                <label>Expiration Date
                    <span class="small">Scheduled date that membership will expire.</span>
                </label>
                <input type="text" id="expiration_date" />
-->

            </form>

        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
