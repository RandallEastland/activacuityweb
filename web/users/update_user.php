<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Update User</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:730px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:450px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script type="text/javascript">
   
        window.onload = function getUserInfo() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = document.URL.split('=')[1];
            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = JSON.parse(xmlhttp.responseText);
					console.log(response);
					
                    if (response.result == "success") {
                        
                        document.getElementById("user_name").value          = response.data.user_name;
                        document.getElementById("first_name").value         = response.data.first_name;
                        document.getElementById("last_name").value          = response.data.last_name;
                        document.getElementById("street").value             = response.data.street;
                        document.getElementById("city").value               = response.data.city;
                        document.getElementById("last_renewal_date").value  = response.data.last_renewal_date;
                        document.getElementById("expiration_date").value    = response.data.expiration_date;
						document.getElementById("report_section").value		= response.data.report_section;

                        //set state combobox value
                        var e = document.getElementById("state");
                        var l = e.options.length;
                        for (var i=0; i<l; i++){
                            if (e.options[i].text == response.data.state) {
                                e.options[i].selected = true; 
                                break;
                            }
                        }

                        //set member type combobox value
                        var e = document.getElementById("member_type");
                        var l = e.options.length;
                        for (var i=0; i<l; i++){
                            if (e.options[i].text == response.data.member_type) {
                                e.options[i].selected = true; 
                                break;
                            }
                        }
						
						// set the race combobox
						var race_list = response.data.race_list;
						var select = document.getElementById("race_list");

						// populate the drop down list with the list of races
						for (var i=0; i<race_list.length; i++) {
							var race = race_list[i];
						    var opt = document.createElement("option");
						    opt.text = race.race_name;
						    opt.value = race.race_id;
						    select.appendChild(opt);
						}
						
						// select the active race id from the drop down list
                        for (var i=0; i<select.options.length; i++){
                            if (select.options[i].value == response.data.race_id) {
                                select.options[i].selected = true;
                                break;
                            }
                        }

                        //set include in progress report combobox value
                        var e = document.getElementById("include_in_progress_report");
                        var l = e.options.length;
                        for (var i=0; i<l; i++){
                            if (e.options[i].text == response.data.include_in_progress_report) {
                                e.options[i].selected = true; 
                                break;
                            }
                        }
 
                    } else {
                        document.getElementById("rightsidebox").innerHTML="Failed to retrieve user information.";
                    }
                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/users/get_user.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);
            
            return false;
        }

        function updateUser() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
             
            var user_id             = document.URL.split('=')[1];
            var user_name           = document.getElementById("user_name").value;
            var first_name          = document.getElementById("first_name").value;
            var last_name           = document.getElementById("last_name").value;
            var street              = document.getElementById("street").value;
            var city                = document.getElementById("city").value;
            var last_renewal_date   = document.getElementById("last_renewal_date").value;
            var expiration_date     = document.getElementById("expiration_date").value;
			var report_section		= document.getElementById("report_section").value;
			
			var select = document.getElementById("state");
			for (var i=0; i<select.options.length; i++) {
				opt = select.options[i];
		        if ( opt.selected === true ) {
					var state = opt.value;
					break;
		        }
			}
			
			var select = document.getElementById("member_type");
			for (var i=0; i<select.options.length; i++) {
				opt = select.options[i];
		        if ( opt.selected === true ) {
					var member_type = opt.value;
					break;
		        }
			}
			
			var select = document.getElementById("race_list");
			for (var i=0; i<select.options.length; i++) {
				opt = select.options[i];
		        if ( opt.selected === true ) {
					var race_id = opt.value;
					break;
		        }
			}

            var e = document.getElementById("state");
            var st = e.options[e.selectedIndex].value;

            var e = document.getElementById("member_type");
            var member_type = e.options[e.selectedIndex].value;
			
            var e = document.getElementById("include_in_progress_report");
            var include_in_progress_report = e.options[e.selectedIndex].value;

            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
					console.log(response);

                    if (response == "success") {
                        //alert("Changes to " + first_name + " " + last_name + " updated successfully");
                        window.location.href = "users.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "user_id="              +encodeURIComponent(user_id)+
                            "&user_name="           +encodeURIComponent(user_name)+
                            "&first_name="          +encodeURIComponent(first_name)+
                            "&last_name="           +encodeURIComponent(last_name)+
                            "&street="              +encodeURIComponent(street)+
                            "&city="                +encodeURIComponent(city)+
                            "&st="                  +encodeURIComponent(state)+
                            "&member_type="         +encodeURIComponent(member_type)+
							"&race_id="		  		+encodeURIComponent(race_id)+
                            "&last_renewal_date="   +encodeURIComponent(last_renewal_date)+
							"&include_in_progress_report=" +encodeURIComponent(include_in_progress_report)+
							"&report_section="		+encodeURIComponent(report_section)+
                            "&expiration_date="     +encodeURIComponent(expiration_date);
            
            xmlhttp.open("POST", "../scripts/users/update_user.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
        
        
        function goToDashboard() {
            window.location.href = "../dashboard.html";
        }
    
    </script>

    <div id="titleframe">

        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Update User</h1>
            <p>Generally, users will maintain their own accounts through the app. Use this screen to make any changes you feel are necessary.</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="updateUser()">Update User</button>
            <button type="submit" class="bigbutton" onClick="parent.location='users.php'">All Users</button>
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>

        </div>
        
        <div id="additem" >
                
            <form id="form" name="form" method="post" action="../scripts/create_segment.php">

                <label>User Name
                    <span class="small"></span>
                </label>
                <input type="text" id="user_name" autofocus/>
            
                <label>First Name
                    <span class="small"></span>
                </label>
                <input type="text" id="first_name" />
    
                <label>Last Name
                    <span class="small"></span>
                </label>
                <input type="text" id="last_name" />

                <label>Street Address
                    <span class="small"></span>
                </label>
                <input type="text" id="street" />

                <label>City
                    <span class="small"></span>
                </label>
                <input type="text" id="city" />

                <label>State
                    <span class="small"></span>
                </label>
                <select class="combobox" id="state">
                    <option value="AL">AL</option>
                    <option value="AK">AK</option>
                    <option value="AZ">AZ</option>
                    <option value="AR">AR</option>
                    <option value="CA">CA</option>
                    <option value="CO" selected="selected">CO</option>
                    <option value="CT">CT</option>
                    <option value="DE">DE</option>
                    <option value="DC">DC</option>
                    <option value="FL">FL</option>
                    <option value="GA">GA</option>
                    <option value="HI">HI</option>
                    <option value="ID">ID</option>
                    <option value="IL">IL</option>
                    <option value="IN">IN</option>
                    <option value="IA">IA</option>
                    <option value="KS">KS</option>
                    <option value="KY">KY</option>
                    <option value="LA">LA</option>
                    <option value="ME">ME</option>
                    <option value="MD">MD</option>
                    <option value="MA">MA</option>
                    <option value="MI">MI</option>
                    <option value="MN">MN</option>
                    <option value="MS">MS</option>
                    <option value="MO">MO</option>
                    <option value="MT">MT</option>
                    <option value="NE">NE</option>
                    <option value="NV">NV</option>
                    <option value="NH">NH</option>
                    <option value="NJ">NJ</option>
                    <option value="NM">NM</option>
                    <option value="NY">NY</option>
                    <option value="NC">NC</option>
                    <option value="ND">ND</option>
                    <option value="OH">OH</option>
                    <option value="OK">OK</option>
                    <option value="OR">OR</option>
                    <option value="PA">PA</option>
                    <option value="RI">RI</option>
                    <option value="SC">SC</option>
                    <option value="SD">SD</option>
                    <option value="TN">TN</option>
                    <option value="TX">TX</option>
                    <option value="UT">UT</option>
                    <option value="VT">VT</option>
                    <option value="VA">VA</option>
                    <option value="WA">WA</option>
                    <option value="WV">WV</option>
                    <option value="WI">WI</option>
                    <option value="WY">WY</option>
                </select>

                <label>Member Type
                    <span class="small">Paid vs Introductory</span>
                </label>
                <select class="combobox" id="member_type">
                    <option value="paid">Paid</option>
                    <option value="introductory">Introductory</option>
                </select>

                <label>Race
                    <span class="small">Which race is this user affiliated with?</span>
                </label>
                <select class="combobox" id="race_list"></select>

                <label>Renewal Date
                    <span class="small">Last time the user paid for the app.</span>
                </label>
                <input type="date" id="last_renewal_date" class="datepicker">

                <label>Expiration Date
                    <span class="small">Scheduled date that membership will expire.</span>
                </label>
                <input type="date" id="expiration_date" class="datepicker">

                <label>Include In Progress Report</label>
                <select class="combobox" id="include_in_progress_report">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>

                <label>Report Section
                    <span class="small"></span>
                </label>
                <input type="text" id="report_section" />

            </form>

        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
