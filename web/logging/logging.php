<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title>Experience Logging</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../styles/logging.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

        #bodyframe{
            margin:0 auto;
            width:744px;
            float:center;
            clear:left;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>
		
	</head>
	<body onload="loadData()">

		<input type="hidden" id="bool_builder_tables_received"/>
		<input type="hidden" id="current_coach_user_name"/>
		<input type="hidden" id="log_id"/>
		<input type="hidden" id="athlete_id"/>
		<input type="hidden" id="just_new_logs"/>
		<input type="hidden" id="should_return_to_athletes_table"/>
		
	    <div id="titleframe">
        
	        <div class="logoimage">
	            <img id="mainpic" src="../images/logo.png">
	        </div>
           
	        <div class="titletext" >
	            <h1>Experience Logging</h1>
	            <p>ActivAcuity User Experiences.</p>
	        </div>
	    </div>
	
		<div id="bodyframe">
			<div id="innerframe">
				<div id="menu-buttons">
					<div id="btn-new-logs" class="button selected">New Log Entries</div>
					<div id="btn-my-athletes" class="button unselected">My Athletes</div>
					<div id="btn-profile" class="button unselected">My Profile</div>
					<div id="btn-logout" class="button unselected" onclick="logout()">Logout</div>
				</div>			
				
				<div id="new-logs" class='div-visible data-table'></div>
				<div id="my-athletes" class='div-hidden data-table'></div>
				<div id="one-athlete" class='div-hidden data-table'></div>
				
				<div id="profile" class='div-hidden coach-popup'>
					<div class='popup-title'>
						<h1>Coach Login Profile</h1><br><br>
						Use this area to re-set the values that you use to log into the system.
					</div>
		
					<div class="input-line">
				        <p>User Name</p>
						<input type="text" id="user_name" />
					</div>
					<div class="input-line">
						<p>Password</p>
				        <input type="password" id="password" />
					</div>		
					<div class="input-line">
						<p>Re-enter Password</p>
						<input type="password" id="confirmation">
					</div>
		
					<div class="centered-container">
			            <button id='btn-save-profile' type="button" class="gray-button">SAVE PROFILE</button>
						<button id='btn-close-profile' type="button" class="gray-button">CLOSE</button>
					</div>					
				</div>

				<div id="comment-box" class='div-hidden coach-popup'>
					
					<div class='popup-title'>
						<h1>Comment On Log Entry</h1><br><br>
						Use this to make a comment back to your athlete on their log entry.
					</div>
		
					<div class="original-log"></div>

					<div class="comment">
						<textarea rows='10' cols='50' id='comment-text'/></textarea>
					</div>		
		
					<div class="centered-container">
			            <button id='btn-send-comment' type="button" class="gray-button">SEND COMMENT</button>
						<button id='btn-close-comment' type="button" class="gray-button">CLOSE</button>
					</div>					
				</div>

			</div> <!--innerframe-->
		</div> <!--bodyframe-->
	
		<input type="hidden" id="club_id"/>
		
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script src="logging.js"></script>
		<script src="coach_button_clicks.js"></script>

	</body>
</html>
