
function verifySession() {
 
    var xmlhttp = new XMLHttpRequest();
    var returnValue = 0;
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            
            var session_verified = xmlhttp.responseText;
            
            if (session_verified == 1) {
                returnValue = 1;
            } else {
                // session was not verified, kick the user out to the signin page
                //window.location.href = "/ActivFoKus/signin.php";
                returnValue = 0;
            }
        }
    }
            
    xmlhttp.open("GET", "/scripts/verifySession.php", false);
    xmlhttp.send();

    return returnValue;
}

function refreshSession() {

    var xmlhttp = new XMLHttpRequest();
    var returnValue = 0;
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var response = xmlhttp.responseText;
            //console.log(response);
            if (response == "success") {
                returnValue = 1;
            } else {
                returnValue = 0;
            }
        }
    }
            
    xmlhttp.open("GET", "/scripts/refreshSession.php", true);
    xmlhttp.send();

    return returnValue;
}

function logout() {

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            window.location.href = "/signin.php";
            return false;
        }
    }  

    xmlhttp.open("GET", "/scripts/administration/logout.php", false);
    xmlhttp.send();
    return false;

}

function goToDashboard() {
    window.location.href = "/dashboard.php";
}




    
