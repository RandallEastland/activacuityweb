$(document).ready(function(){
	// trap the coach's current user name in the event that they want to change it later
	document.getElementById('current_coach_user_name').value = getCookie('username');
	document.getElementById('user_name').value = getCookie('username');
	document.getElementById('athlete_id').value = 0;
});

function loadData() {
	getCoachId();
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

$("#btn-save-profile").click(function() {
	
	var current_user_name = document.getElementById('current_coach_user_name').value;
    var new_user_name 	  = document.getElementById("user_name").value;
    var password 	 	  = document.getElementById("password").value;
    var confirmation 	  = document.getElementById("confirmation").value;

	if (user_name.length == 0) {
		alert("Please enter a user name!");
		return;
	}
	
	if (password.length == 0) {
		if (!confirm("You have not entered a password. This will leave the password unchanged. Are you sure?")) {
			return;
		}
	} else if (password != confirmation) {
		alert("Your password entries do not match!");
		return;
	}

    var xmlhttp=new XMLHttpRequest();
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var response = xmlhttp.responseText;
            // console.log(response);
            if (response == "success") {
                alert("Profile Updated!");
				hideAllSections();
            } else {
                alert("Profile Update Failed!");
            }
        }
    }

    var content =   "current_user_name=" +encodeURIComponent(current_user_name)+
                    "&new_user_name=" 	 +encodeURIComponent(new_user_name)+
					"&password="  		 +encodeURIComponent(password);
    
    xmlhttp.open("POST", "../scripts/coaches/update_coach_profile_without_id.php", true);
    xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(content);

    return false;
});

function getCoachId() {

	var user_name = getCookie('username');
    var xmlhttp=new XMLHttpRequest();
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var response = JSON.parse(xmlhttp.responseText);
            if (response.result == "success") {
                document.getElementById("coach_id").value = response.message.coach_id;
				document.getElementById("club_id").value = response.message.club_id;
				buildTables(response.message.coach_id, response.message.club_id);
            }
        }
    }

    var content =   "user_name=" +encodeURIComponent(user_name);
    
    xmlhttp.open("POST", "../scripts/coaches/getCoachIdFromUserName.php", true);
    xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(content);

    return false;	
}

function buildTables(coach_id, club_id) {
	
    var xmlhttp=new XMLHttpRequest();
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var response = JSON.parse(xmlhttp.responseText);
			// console.log(xmlhttp.responseText);
            if (response.result == "success") {
				document.getElementById("new-logs").innerHTML = response.all_tables.new_logs;
                document.getElementById("my-athletes").innerHTML = response.all_tables.athletes;
	        } else {
            	document.getElementById('my-athletes').innerHTML = "<h3>Query Failed</h3>";
            }
        }
    }

    var content =   "coach_id=" +encodeURIComponent(coach_id)+
					"&club_id=" +encodeURIComponent(club_id);
    
    xmlhttp.open("POST", "../scripts/coaches/buildTablesForCoach.php", true);
    xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(content);

    return false;	
}

function getLogsForOneAthlete (button_id, just_new_logs) {

	var athlete_id = button_id.split('-')[1];
	var club_id = document.getElementById("club_id").value;
	
	document.getElementById('athlete_id').value = athlete_id;
	document.getElementById('just_new_logs').value = just_new_logs;

    var xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function() {
	    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			// console.log(xmlhttp.responseText);
	        var response = JSON.parse(xmlhttp.responseText);
			// console.log(xmlhttp.responseText);
	        if (response.result == "success") {
				document.getElementById("one-athlete").innerHTML = response.table_string;
	        } else {
	        	document.getElementById('one-athlete').innerHTML = "<h3>Query Failed</h3>";
	        }

			// swap the visible view
			document.getElementById("one-athlete").setAttribute('class', 'div-visible data-table');
			document.getElementById("my-athletes").setAttribute("class", "div-hidden data-table");
			document.getElementById("btn-my-athletes").style.textDecoration = "none";

			if (just_new_logs == 1)
				document.getElementById("subtitle").innerHTML = "New Workout Logs For " + response.athlete_name;
			else
				document.getElementById("subtitle").innerHTML = "All Workout Logs For " + response.athlete_name;
	    }
	}

    var content =   "athlete_id="     +encodeURIComponent(athlete_id)+
					"&club_id="       +encodeURIComponent(club_id)+
					"&just_new_logs=" +encodeURIComponent(just_new_logs);

    xmlhttp.open("POST", "../scripts/coaches/getLogTableForOneAthlete.php", true);
    xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(content);

    return false;
}

function markRead(btn_id) {

	var log_id 		  = btn_id.split('-')[1];
	var coach_id 	  = document.getElementById('coach_id').value;
	var club_id 	  = document.getElementById('club_id').value;
	var just_new_logs = document.getElementById('just_new_logs').value;
	var athlete_id    = document.getElementById('athlete_id').value;
	
    var xmlhttp=new XMLHttpRequest();
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var response = JSON.parse(xmlhttp.responseText);
			// console.log(xmlhttp.responseText);
            if (response.result == "success") {
				document.getElementById('new-logs').innerHTML = response.updated_coach_table;
				document.getElementById('one-athlete').innerHTML = response.updated_athlete_table;
	        } else {
				alert("A problem occurred");
            }
        }
    }

    var content =   "log_id="         +encodeURIComponent(log_id)+
					"&coach_id="      +encodeURIComponent(coach_id)+
					"&athlete_id="    +encodeURIComponent(athlete_id)+
					"&just_new_logs=" +encodeURIComponent(just_new_logs)+
					"&club_id="	      +encodeURIComponent(club_id);
	
    xmlhttp.open("POST", "../scripts/coaches/markLogAsRead.php", true);
    xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(content);

    return false;	
}

function showReply (btn_id) {
	document.getElementById('log_id').value = btn_id.split('-')[1];
	showCommentBox();
}

function sendCommentToServer() {
	
	var log_id   	  = document.getElementById('log_id').value;
	var coach_id 	  = document.getElementById('coach_id').value;
	var athlete_id    = document.getElementById('athlete_id').value;
	var just_new_logs = document.getElementById('just_new_logs').value;
	var club_id  	  = document.getElementById('club_id').value;
	var comment  	  = document.getElementById('comment-text').value;
	
    var xmlhttp=new XMLHttpRequest();
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			// console.log(xmlhttp.responseText);
            var response = JSON.parse(xmlhttp.responseText);
			// console.log(xmlhttp.responseText);
            if (response.result == "success") {
				
				document.getElementById('new-logs').innerHTML = response.updated_coach_table;
				document.getElementById('one-athlete').innerHTML = response.updated_athlete_table;
				document.getElementById('comment-text').value = "";
				
				alert("Comment Posted");
				
				hideCommentBox(); // in coach_button_clicks.js
				
				if (document.getElementById('should_return_to_athletes_table').value == 'yes') {
					showOneAthlete();  // in coach_button_clicks.js
				} else {
					$("#btn-new-logs").click();
				}
				
	        } else {
				alert("A problem occurred");
            }
        }
    }

    var content =   "log_id="    	  +encodeURIComponent(log_id)+
					"&coach_id=" 	  +encodeURIComponent(coach_id)+
					"&athlete_id=" 	  +encodeURIComponent(athlete_id)+
					"&just_new_logs=" +encodeURIComponent(just_new_logs)+
					"&club_id="	 	  +encodeURIComponent(club_id)+
					"&comment="  	  +encodeURIComponent(comment);
	
    xmlhttp.open("POST", "../scripts/coaches/sendLogCommentToServer.php", true);
    xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(content);

    return false;	
}