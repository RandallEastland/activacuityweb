function generateDistributionList() {
	
    //check for excessive inactivity
    if (verifySession()) {
        console.log("verified");
    } else {
        console.log("verification failed");
        return false;
    }
	
    document.getElementById("distribution_list").innerHTML="Generating Distribution List. Please wait...";

    var xmlhttp = new XMLHttpRequest();
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            document.getElementById("distribution_list").innerHTML = xmlhttp.responseText;
        }
    }
    
    xmlhttp.open("GET", "http://www.activacuity.com/scripts/users/generate_distribution_list.php", true);
    xmlhttp.send();
    return false;
}