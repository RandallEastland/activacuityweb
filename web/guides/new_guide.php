<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>New Guide</title>
    <link rel="icon" type="image/png" href="images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }
    </style>

    <script type="text/javascript" src="../scripts/tinymce/tinymce.min.js"></script>
    
    <script type="text/javascript">tinymce.init({
            selector    : "textarea",
            toolbar : false,
            menubar : "edit format",
            forced_root_block : 'div'
        });
    </script>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script type="text/javascript">

        window.onload = function setDefauluts() {
            document.getElementById("parent_id").value = 0;
        }

        function createGuide() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
             
            var guide_num   = document.getElementById("guide_num").value;
            var description = document.getElementById("description").value;
            var parent_id   = document.getElementById("parent_id").value;
            var elaboration = tinyMCE.get("elaboration").getContent();
            
            var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response == "success") {
                        var successResponse =  "Segment successfully added.";
                        document.getElementById("rightsidebox").innerHTML=successResponse;

                        //clear the text fields for next input
                        var next_guide_num = parseInt(document.getElementById("guide_num").value) + 1;
                        document.getElementById("guide_num").value = next_guide_num;
                        document.getElementById("description").value = "";
                        document.getElementById("elaboration").value = "";

                        document.getElementById("guide_num").focus();
                        
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "guide_num="    +encodeURIComponent(guide_num)+
                            "&description=" +encodeURIComponent(description)+
                            "&parent_id="   +encodeURIComponent(parent_id)+
                            "&elaboration=" +encodeURIComponent(elaboration);
                
            xmlhttp.open("POST", "../scripts/guides/create_guide.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
    
        function goToDashboard() {
            window.location.href = "../dashboard.php";
        }
    
    </script>

    <div id="titleframe">

        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>New Guide</h1>
            <p>Use this area to add a new segment.</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="createGuide()">Add Guide</button>
            <button type="submit" class="bigbutton" onClick="parent.location='guides.php'">All Guides</button>
            <button type="submit" class="bigbutton" onClick="goToDashboard()">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>
        
        <div id="additem" >
                
                <label>Guide Number
                    <span class="small">Assign a sorting number for this guide.</span>
                </label>
                <input type="text" id="guide_num" autofocus/>
            
                <label>Description
                    <span class="small">This will appear in the app.</span>
                </label>
                <input type="text" id="description" />

                <label>Parent
                    <span class="small">Enter 0 if this appears on the main screen.</span>
                </label>
                <input type="text" id="parent_id" />

                <h3>What information do you want to give to the user?</h3>
                <textarea id="elaboration" rows="20" cols="50"></textarea>

        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
