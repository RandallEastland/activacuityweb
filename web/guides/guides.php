<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Guides</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

        #bodyframe{
            margin:0 auto;
            width:688px;
            float:center;
            clear:left;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script language="javascript" type="text/javascript">

        function deleteThis(btn_id, guide_num) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            var id = btn_id.split('-')[1];

            //confirm intention to delete
            var question = "Are you sure that you want to delete this guide?";            
            if (!confirm(question)) {return;}
	        var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    //console.log(response);
                    
                    //check to see if this segment is referred to by a program
                    if (response.split("|")[0] == "dependency") {
                        console.log(response);
                        var numDependencies = response.split("|")[1];
                        var alertText = "Deletion canceled. This guide is referred to by at least ";
                        alertText += numDependencies + " other guide(s).";
                        alert(alertText);
                        return; 
                    }
 
                    if (response == "success") {
                        window.location.reload();
                    }
                }
            }

            var content = "id="         +encodeURIComponent(id)+
                          "&guide_num=" +encodeURIComponent(guide_num);
            
            xmlhttp.open("POST", "../scripts/guides/delete_guide.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
 
        function updateThis(id) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = id.split('-')[1];
            
            var next = "update_guide.php?id="+id;
            
            window.location.href = next;
            
        }
        
        function addNewGuide() {
            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            window.location.href = "new_guide.php";
        }
    
    </script>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Guides</h1>
            <p>Use this area to update the list of app guides.</p>
        </div>
    </div>
    
    <div id="bodyframe">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="addNewGuide()">Add New Guide</button>          
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>


        <table id="dataset">
            <tr class="dataheader" valign='bottom'>
                <td align='center'>Guide Number</td>
                <td>Description</td>
                <td align='center'>Parent List</td>
                <td align='center'>+</td>
                <td align='center'>-</td>
            </tr>

            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $connection = $path . '/scripts/connection.php';
                include ($connection);

                $sql="SELECT * FROM guides ORDER BY parent_id, guide_num";
                $result = mysqli_query($mysqli, $sql);
                if (!$result) {die("SQL error retrieving guide lists.");}

                if (mysqli_num_rows($result) == 0) {
                    echo "
                        The list of guides is empty.

                        ";
                    die();
                }
                    
                while ($row = mysqli_fetch_array($result)) {

                    $guide_num   = $row['guide_num'];
                    $description = $row['description'];
                    $parent_id   = $row['parent_id']; 
                    
                    //make unique names for each <td> in this row
                    $guide_id  = $row['guide_id']; //this is the id for this guide in the database 
                    
                    $id_hdr          = "id_hdr-"            . $guide_id;
                    $guide_num_hdr   = "guide_num_hdr-"     . $guide_id;
                    $description_hdr = "description_hdr-"   . $guide_id;
                    $parent_hdr      = "parent_hdr-"        . $guide_id;
                    $btn_update      = "btn_update-"        . $guide_id;
                    $btn_delete      = "btn_delete-"        . $guide_id;
                    
                    //Build this row of the table
                    $tableString = 
                        "<tr class='datarow'>"  .  
                            "<td class='id'                 id='$id_hdr' style='display:none'>" . $row['guide_id']    . "</td>" .
                            "<td class='xxx_center'         id='$guide_num_hdr'>"               . $row['guide_num']   . "</td>" .
                            "<td class='cccl'               id='$description_hdr'>"             . $row['description'] . "</td>" .
                            "<td class='xxx_center          id='$parent_hdr'>"                  . $row['parent_id']   . "</td>" .
                       
                            "<td align='center'>" .
                                "<image class='xv' type='image' src='../images/update.png' id='$btn_update' 
                                    onclick='updateThis(this.id)'>".
                            "</td>" .
                       
                            "<td align='center'>" .
                            "<image class='xv' type='image' src='../images/delete.png' id='$btn_delete' 
                                    onclick='deleteThis(this.id, \"$guide_num\")'>".
                            "</td>" .
                        "</tr>";
                        
                    echo $tableString;
                }    
           
                mysqli_close($mysqli);
            ?>

        </table>

    </div>

</body>

</html>
