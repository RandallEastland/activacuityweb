<?php
session_start();

// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Update Program</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

</head>

<body>

    <script type="text/javascript">
   
        window.onload = function getProgramInfo() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            var id = document.URL.split('=')[1];
            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response != "failure") {
                        
                        var fieldsArray = response.split('|');
 
                        var segment_id      = fieldsArray[0];
                        var program_number  = fieldsArray[1];
                        var description     = fieldsArray[2];
                        
                        //select the correct value from the list name combobox
                        var e = document.getElementById("segment_id");
                        var l = e.options.length;
                        for (var i=0; i<l; i++){
                            if (e.options[i].value == segment_id) {
                                e.options[i].selected = true; 
                                break;
                            }
                        }
 
                        document.getElementById("program_number").value = program_number;
                        document.getElementById("description").value    = description;
                        
                    } else {
                        document.getElementById("rightsidebox").innerHTML="Failed to retrieve program information.";
                    }
                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/programs/get_program.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);
            
            return false;
        }

        function updateProgram() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            var id              = document.URL.split('=')[1];
            var program_number  = document.getElementById("program_number").value;
            var description     = document.getElementById("description").value;

            //select the correct value from the list name combobox
            var e = document.getElementById("segment_id");
            var segment_id = e.options[e.selectedIndex].value; //this is the list id (an integer)            

            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response == "success") {
                        //alert("Changes to " + name + " updated successfully");
                        window.location.href = "programs.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML="Failed to update program information.";
                    }
                }
            }

            var content =   "id="               +encodeURIComponent(id)+
                            "&segment_id="      +encodeURIComponent(segment_id)+
                            "&program_number="  +encodeURIComponent(program_number)+
                            "&description="     +encodeURIComponent(description);
            
            xmlhttp.open("POST", "../scripts/programs/update_program.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
        
        
        function goToDashboard() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            window.location.href = "../dashboard.html";
        }
    
    </script>

    <div id="titleframe">

        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Update Program</h1>
            <p>Make any changes that you wish - then click Update Program</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="updateProgram()">Update Program</button>
            <button type="submit" class="bigbutton" onClick="parent.location='programs.php'">All Programs</button>
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>

        </div>
        
        <div id="additem" >
                
            <form id="form" name="form" method="post" action="programs/scripts/create_program.php">

                <label>List
                    <span class="small">Which list does this program belong to?</span>
                </label>
                
                <?php

                    $path = $_SERVER['DOCUMENT_ROOT'];
                    $connection = $path . '/scripts/connection.php';
                    include ($connection);

                    $sql="SELECT * FROM segments ORDER BY segment_id";
                    $result = mysqli_query($mysqli, $sql);
                    if (!$result) {die("SQL error retrieving segments.");}

                    echo "<select id='segment_id' class='combobox'>";
                    
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value='" . $row['segment_id'] . "'>" . $row['segment_name'] . "</option>";
                    }

                    echo "</select>";

                    mysqli_close($mysqli);
                ?>

                <label>Program Number
                    <span class="small">Assign a number to this program.</span>
                </label>
                <input type="text" name="program_number" id="program_number" />
    
                <label>Program Description
                    <span class="small">This will appear on the app.</span>
                </label>
                <input type="text" name="description" id="description" />
              
            </form>

        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
