<?php
session_start();

// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Programs</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

        #bodyframe{
            margin:0 auto;
            width:744px;
            float:center;
            clear:left;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

</head>

<body>

    <script type="text/javascript">

        function deleteThis(btn_id) {
            
            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            var id = btn_id.split('-')[1];

            //confirm intention to delete
            var question = "Are you sure that you want to delete this program?";            
            if (!confirm(question)) {return;}
	        var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);

                    //check to see if this program has any child sessions
                    if (response.split("|")[0] == "dependency") {
                        var numDependencies = response.split("|")[1];
                        var alertText = "Deletion canceled. This program has at least ";
                        alertText += numDependencies + " session(s) associated with it. Please delete these first.";
                        alert(alertText);
                        return; 
                    }
                    
                    if (response == "success") {
                        window.location.reload();
                    }
                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/programs/delete_program.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);
  
            return false;
        }
 
        function updateThis(id) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = id.split('-')[1];
            
            var next = "update_program.php?id="+id;
            
            window.location.href = next;
            
        }
        
        function addNewProgram() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            window.location.href = "new_program.php";
        }

        function viewSessions(id, description) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            window.location.href = "../sessions/sessions.php?id="+id+"&description="+description;
        }
        
    </script>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Programs</h1>
            <p>Use this area to update the list of programs.</p>
        </div>
    </div>
    
    <div id="bodyframe">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="addNewProgram()">Add New Program</button>          
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>


        <table id="dataset">
            <tr class="dataheader" valign='bottom'>
                <td>Segment</td>
                <td align='center'>Program Number</td>
                <td>Description</td>
                <td align='center'>Edit Sessions</td>
                <td align='center'>+</td>
                <td align='center'>-</td>
            </tr>

            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $connection = $path . '/scripts/connection.php';
                include ($connection);

                //$sql="SELECT * FROM programs ORDER BY segment_num, prog_num";
                $sql = "SELECT prog_id, segment_id, segment_name, prog_num, description
                        FROM programs
                            JOIN segments USING (segment_id)
                        ORDER BY segment_id, prog_num";
                $result = mysqli_query($mysqli, $sql);
                if (!$result) {die("SQL error retrieving program lists.");}

                if (mysqli_num_rows($result) == 0) {
                    echo "
                        The list of programs is empty.

                        ";
                    die();
                }
                    
                $sql = "SELECT * FROM segments ORDER BY segment_num";
                $segments = mysqli_query($mysqli, $sql);
                if (!$segments) {die("SQL error retrieving segments.");}

                while ($row = mysqli_fetch_array($result)) {
 
                    $segment_name = $row['segment_name'];
                    $prog_num     = $row['prog_num'];
                    $description  = $row['description'];
                    
                    //make unique names for each <td> in this row
                    $program_id  = $row['prog_id']; //this is the id for this coach in the database 
                    
                    $id_hdr          = "id_hdr-"          . $program_id;
                    $segment_hdr     = "segment_hdr-"     . $program_id;
                    $prog_num_hdr    = "prog_num_hdr-"    . $program_id;
                    $description_hdr = "description_hdr-" . $program_id;
                    $sessions_hdr    = "sessions_hdr-"    . $program_id;
                    $btn_update      = "btn_update-"      . $program_id;
                    $btn_delete      = "btn_delete-"      . $program_id;
                    
                    //Build this row of the table
                    $tableString = 
                        "<tr class='datarow'>"  .  
                            "<td class='id'                id='$id_hdr' style='display:none'>" . $row['id']          . "</td>" .
                            "<td class='cxxv'              id='$segment_hdr'>"                 . $segment_name          . "</td>" .
                            "<td class='xv' align='center' id='$prog_num_hdr'>"                . $row['prog_num']    . "</td>" .
                            "<td class='cc'                id='$description_hdr'>"             . $row['description'] . "</td>" .

                            "<td class='c0' align='center'>" .
                                "<button type='button' class='sessions_button' id='$sessions_hdr' 
                                    onclick='viewSessions(\"$program_id\", \"$description\")'>Edit Sessions
                                </button>".
                            "</td>" .
                            
                            "<td class='xv' align='center'>" .
                                "<image class='xv' type='image' src='../images/update.png' id='$btn_update' 
                                    onclick='updateThis(this.id)'>".
                            "</td>" .
                       
                            "<td class='xv' align='center'>" .
                                "<image class='xv' type='image' src='../images/delete.png' id='$btn_delete' 
                                    onclick='deleteThis(this.id, \"$description\")'>".
                            "</td>" .
                        "</tr>";
                        
                    echo $tableString;
           
                }    
           
                mysqli_close($mysqli);
            ?>

        </table>

    </div>

</body>

</html>
