<?php
session_start();

// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>New Program</title>
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }
    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>
</head>

<body>

    <script type="text/javascript">

        function createProgram() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var program_number = document.getElementById("program_number").value;
            var description    = document.getElementById("description").value;
            
            //select the correct value from the list name combobox
            var e = document.getElementById("segment_id");
            var segment_id = e.options[e.selectedIndex].value; //this is the list id (an integer)  
            
            var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    //console.log(xmlhttp.responseText.length);
                    if (response == "success") {
                        var successResponse =  "Program successfully added.";
                        document.getElementById("rightsidebox").innerHTML=successResponse;

                        //clear the text fields for next input
                        var last_prog_num = parseInt(document.getElementById("program_number").value);
                        document.getElementById("program_number").value = last_prog_num + 1;
                        document.getElementById("description").value = "";

                        document.getElementById("program_number").focus();

                        //e.selectedIndex = 0; 
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "segment_id="      +encodeURIComponent(segment_id)+
                            "&program_number=" +encodeURIComponent(program_number)+
                            "&description="    +encodeURIComponent(description);
                
            xmlhttp.open("POST", "../scripts/programs/create_program.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
    
        function goToDashboard() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            window.location.href = "../dashboard.php";
        }

        function focusOnProgNum() {
            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();

            document.getElementById("program_number").focus();
        }

    </script>

    <div id="titleframe">

        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>New Program</h1>
            <p>Use this area to add a new program.</p>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="createProgram()">Add Program</button>
            <button type="submit" class="bigbutton" onClick="parent.location='programs.php'">All Programs</button>
            <button type="submit" class="bigbutton" onClick="goToDashboard()">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>
        
        <div id="additem" >
                
                <label>List
                    <span class="small">Which list does this program belong to?</span>
                </label>

                <?php

                    $path = $_SERVER['DOCUMENT_ROOT'];
                    $connection = $path . '/scripts/connection.php';
                    include ($connection);

                    $sql="SELECT * FROM segments ORDER BY segment_num";
                    $result = mysqli_query($mysqli, $sql);
                    if (!$result) {die("SQL error retrieving segments.");}

                    echo "<select id='segment_id' class='combobox' onchange=\"if (this.selectedIndex) focusOnProgNum()\";>";
                    
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value='" . $row['segment_id'] . "'>" . $row['segment_name'] . "</option>";
                    }

                    echo "</select>";

                    mysqli_close($mysqli);
                ?>
            
                <label>Program Number
                    <span class="small">Assign a number to this program.</span>
                </label>
                <input type="text" name="program_number" id="program_number" autofocus/>

 
                <label>Program Description
                    <span class="small">This will appear in the app.</span>
                </label>
                <input type="text" name="description" id="description" />
              
        </div>

    </div>
    
    <div id="rightsidebox"></div>

</body>

</html>
