<?php
session_start();

// verify that an active session exists before loading the page
$path = $_SERVER['DOCUMENT_ROOT'];
$session_verifier = $path . '/scripts/administration/session_verifier.php';
include ($session_verifier);
if (session_verified() == '0') 
    header( 'Location: ../signin.php' ) ;
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Sessions</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

        #program_name{
            width:450px;
            color:#0055b7;
            font-size:large;
            margin-left:0px;
            margin-top:15px;
            margin-bottom:15px;
        }

    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>
</head>

<body>

    <script type="text/javascript">

        window.onload = function getProgram() {

            var prog_name = document.URL.split('=')[2];
            prog_name = unescape(prog_name); // replaces any %20 with whitespace
            //console.log(prog_name);
            document.getElementById("program_name").innerHTML = "Program: " + prog_name;
            return true;
        }
        
        function deleteThis(btn_id) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
  
            var id = btn_id.split('-')[1];
            
            //confirm intention to delete
            var question = "Are you sure that you want to delete this session?";            
            if (!confirm(question)) {return;}
	        var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;

                    if (response == "success") {
                        window.location.reload();
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }

                }
            }

            var content = "id=" + encodeURIComponent(id);
            
            xmlhttp.open("POST", "../scripts/sessions/delete_session.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);
             
            return false;
        }
 
        function updateThis(id, list, program_number, description, media_url) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var id = id.split('-')[1];
            
            var next = "update_segment.php?id="+id;
            
            window.location.href = next;
            
        }

        function saveNewSession(bool_new, session_id, prog_id, session_num_hdr, media_file_hdr, time_hdr) {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
            
            var session_num = parseInt(document.getElementById(session_num_hdr).value);
            var media_filename = document.getElementById(media_file_hdr).value;
            var seconds = document.getElementById(time_hdr).value;
            
            var xmlhttp=new XMLHttpRequest();

            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;

                    if (response == "success") {
                        window.location.reload();
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }


            var content;
            var post_string;
            
            if (bool_new) { 
            
                content =   "session_num="   +encodeURIComponent(session_num)+
                            "&prog_id="      +encodeURIComponent(prog_id)+
                            "&media_file="   +encodeURIComponent(media_filename)+
                            "&time_seconds=" +encodeURIComponent(seconds);
            
                post_string = "../scripts/sessions/create_session.php";
           
            } else {
           
                content =   "session_id="    +encodeURIComponent(session_id)+
                            "&prog_id="      +encodeURIComponent(prog_id)+
                            "&session_num="  +encodeURIComponent(session_num)+
                            "&media_file="   +encodeURIComponent(media_filename)+
                            "&time_seconds=" +encodeURIComponent(seconds);
           
                post_string = "../scripts/sessions/update_session.php";

            }
            
            xmlhttp.open("POST", post_string, true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;

        }
        
    </script>

    <div id="titleframe">
        
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>
           
        <div class="titletext" >
            <h1>Sessions</h1>
            <p>Use this area to update the sessions for this program.</p>
        </div>
    </div>
    
    <div id="userentry">
    
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="parent.location='../programs/programs.php'">All Programs</button>
            <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Return to Dashboard</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>

        </div>

        <div id="additem"> 
            <div id="program_name"><label></label></div>
        
            <table id="dataset">
                <tr class="dataheader" valign='bottom'>
                    <td align='center'>Session Number</td>
                    <td>Media Filename</td>
                    <td align='center'>Play Time in Seconds</td>
                    <td align='center'>+</td>
                    <td align='center'>-</td>
                </tr>

                <?php
                    $path = $_SERVER['DOCUMENT_ROOT'];
                    $connection = $path . '/scripts/connection.php';
                    include ($connection);

                    $prog_id = $_GET['id'];

                    function addLastRow($prog_id, $session_num_hdr, $media_file_hdr, $time_hdr, $btn_save) {
                        
                        // set up a blank row for the user to add a session
                        $tableString = 
                            "<tr class='inputrow'>" .
                                "<td class='xxx'>" .
                                    // note that the session number is always 1 in this case
                                    "<input class='xxx' type='text' id='$session_num_hdr' autofocus/>" .  
                                "</td>" .

                                "<td class='cc'>" . 
                                    "<input class='cc' type='text' id='$media_file_hdr'" . 
                                "</td>" .

                                "<td class='l0'>" . 
                                    "<input class='l0' type='text' id='$time_hdr' 
                                        onkeydown='if (event.keyCode==13) 
                                            saveNewSession(true, -1, \"$prog_id\", \"$session_num_hdr\", \"$media_file_hdr\", \"$time_hdr\")'/>" .
                                "</td>" .
                                
                                "<td align='center'>" .
                                    "<image class='xxx' type='image' src='../images/save.png' id='$btn_save'
                                        onclick='saveNewSession(true, -1, \"$prog_id\", \"$session_num_hdr\", \"$media_file_hdr\", \"$time_hdr\")'>".
                                "</td>" .
                            "</tr>";
                        
                        return $tableString;

                    }

                    echo "Use the empty row at the end to add a new session.<br><br>";

                    $sql="SELECT * FROM sessions WHERE prog_id='$prog_id' ORDER BY session_num";
                    $result = mysqli_query($mysqli, $sql);
                    if (!$result) {die("SQL error retrieving session list.");}

                    if (mysqli_num_rows($result) == 0) {
                        echo "There are currently no sessions set up for this program.<br><br>";

                        $session_num_hdr = "session_num_hdr-1";
                        $media_file_hdr  = "media_file_hdr-1";
                        $time_hdr        = "time_hdr-1";
                        $btn_save        = "btn_save-1";
                        
                        echo addLastRow($prog_id, $session_num_hdr, $media_file_hdr, $time_hdr, $btn_save);
                        echo "<div id='rightsidebox'></div>";  
                        die();
                    }
                    
                    while ($row = mysqli_fetch_array($result)) {
    
                        $session_id  = $row['session_id'];
                        $prog_id     = $row['prog_id'];
                        $session_num = $row['session_num'];
                        $media_file  = $row['media_file']; 
                        $seconds     = $row['play_seconds'];
                    
                        //make unique names for each <td> in this row
                        $session_id  = $row['session_id']; //this is the id for this segment in the database 
                    
                        $id_hdr          = "id_hdr-"          . $session_id;
                        $session_num_hdr = "session_num_hdr-" . $session_id;
                        $media_file_hdr  = "media_file_hdr-"  . $session_id;
                        $time_hdr        = "time_hdr-"        . $session_id;
                        $btn_update      = "btn_update-"      . $session_id;
                        $btn_delete      = "btn_delete-"      . $session_id;
                    
                        //Build this row of the table
                        $tableString = 
                            "<tr class='inputrow'>"  .  
                                "<td class='id' id='$id_hdr' style='display:none'>" . $row['session_id']  . "</td>" .
                                
                                "<td class='xxx'>" .
                                    "<input class='xxx' type='text' id='$session_num_hdr' value='$session_num'/>" . 
                                "</td>" .
                                    
                                    
                                "<td class='cc'>" .
                                    "<input class='cc' type='text' id='$media_file_hdr' value='$media_file'/>" .
                                "</td>" .
                       
                                "<td class='l0'>" .
                                    "<input class='l0' type='text' id='$time_hdr' value='$seconds'/>" .
                                "</td>" .
                       
                                "<td align='center'>" .
                                    "<image class='xxx' type='image' src='../images/save.png' id='$btn_update' 
                                        onclick='saveNewSession(false, \"$session_id\", \"$prog_id\", \"$session_num_hdr\", 
                                            \"$media_file_hdr\", \"$time_hdr\")'>".
                                "</td>" .
                       
                                "<td align='center'>" .
                                    "<image class='xxx' type='image' src='../images/delete.png' id='$btn_delete' 
                                        onclick='deleteThis(this.id, \"$description\")'>".
                                "</td>" .
                               
                            "</tr>";
                        
                        echo $tableString;
                    }    
           
                    mysqli_close($mysqli);

                    $session_id = $session_id + 1;
                    $session_num_hdr = "session_num_hdr-" . $session_id;
                    $media_file_hdr  = "media_file_hdr-"  . $session_id;
                    $time_hdr        = "time_hdr-"        . $session_id;
                    $btn_update      = "btn_update-"      . $session_id;

                    echo addLastRow($prog_id, $session_num_hdr, $media_file_hdr, $time_hdr, $btn_save);

                ?>

            </table>
        <div id="rightsidebox"></div>
        </div>


    </div>

</body>

</html>
