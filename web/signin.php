<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Sign In</title>
    <link rel="icon" type="image/png" href="images/logo.png" />
    <link rel="stylesheet" type="text/css" href="styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }
    
    </style>

</head>

<body>

    <script type="text/javascript">
        
        function attemptLogin() {
        
            var username = document.getElementById("name").value;
            var plaintext = document.getElementById("password").value;
	        var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    if (response == "success") {
                        window.location.href = "dashboard.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "username="     +encodeURIComponent(username)+
                            "&plaintext="   +encodeURIComponent(plaintext);    
            
            xmlhttp.open("POST", "scripts/administration/login.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }

    </script>

    
    <div id="titleframe" >
        <div class="logoimage" >            
            <img id="mainpic" src="images/logo.png">
        </div>

        <div class="titletext">
            <img class="title_image" id="mainpic" src="images/title.png">
            <h1>Administrator Login</h1>
        </div>
    </div>

    <div id="userentry">
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="attemptLogin()">Log In</button>          
        </div>
        
        <div id="additem">
        
            <label>Username
                <span class="small">Enter your username</span>
            </label>
            <input type="text" name="username" id="name" autofocus="autofocus" />

            <label>Password
                <span class="small">Enter your password</span>
            </label>
            <input type="password" name="pass" id="password" onkeydown="if (event.keyCode==13) attemptLogin()" />

        </div>

    </div>

    <div id="rightsidebox"></div>

</body>

</html>
