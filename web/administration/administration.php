<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Database Administration</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">

        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <div id="titleframe">
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>

        <div class="titletext">
            <img id="mainpic" src="../images/title.png" height="79" width="354">
            <!--<h1>Database Administrator Maintenance</h1>-->
            <p>Database Administrator Maintenance</p>
        </div>
    </div>

    <div width="416px">
        <table class="two_button_row">
            <tr>
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='change_password.php'">Change Password</button>
                </td>
                    
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='new_admin.php'">Add New Administrator</button>
                </td>
        </table>

        <table class="one_button_row">
                <td>
                    <button type="submit" class="bigbutton" onClick="parent.location='../dashboard.php'">Cancel</button>
                </td>

            </tr>
        </table>

    </div>  
</body>

</html>
