<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Change Password</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
        #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>
    
</head>

<body>

    <script type="text/javascript">

        function attemptPasswordChange() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
   
            var user_name             = document.getElementById("user_name").value;
            var current_plaintext     = document.getElementById("current_plaintext").value;
            var new_plaintext         = document.getElementById("new_plaintext").value;
            var confirm_plaintext = document.getElementById("confirm_plaintext").value;
            
	        var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    
                    if (response == "Too many attempts") {
                        window.location.href = "signin.html";
                        return false;
                    }
                    
                    if (response == "success") {
                        alert("Password Successfully Changed");
                        window.location.href = "../dashboard.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "user_name="             +encodeURIComponent(user_name)+
                            "&current_plaintext="    +encodeURIComponent(current_plaintext)+
                            "&new_plaintext="        +encodeURIComponent(new_plaintext)+
                            "&confirm_plaintext="    +encodeURIComponent(confirm_plaintext);
            
            xmlhttp.open("POST", "../scripts/administration/change_password.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }

    </script>

    <div id="titleframe" >
        <div class="logoimage" >            
            <img id="mainpic" src="../images/logo.png">
        </div>

        <div class="titletext">
            <h1>Change Password</h1>
            <p>Change your administrator password</p>
        </div>
    </div>

    <div id="userentry" >
        
        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="attemptPasswordChange()">Change Password</button>          
            <button type="submit" class="bigbutton" onClick="parent.location='administration.php'">Cancel</button>
            <button type="submit" class="bigbutton" onClick="logout()">Log Out</button>
        </div>
        
        <div id="additem">

            <label>Username
                <span class="small">Enter your user name</span>
            </label>
            <input type="text" id="user_name" autofocus="autofocus" />

            <label>Current Password
                <span class="small">Enter your current password</span>
            </label>
            <input type="password" id="current_plaintext" />

            <label>New Password
                <span class="small">Enter your new password</span>
            </label>
            <input type="password" id="new_plaintext" />
            
            <label>Confirm New Password
                <span class="small">Confirm your new password</span>
            </label>
            <input type="password" id="confirm_plaintext" onkeydown="if (event.keyCode==13) attemptPasswordChange()" />

        </div>
        
    </div>
        
    <div id="rightsidebox"></div>
        
</body>

</html>
