<?php
session_start();
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>New Administrator</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <link rel="stylesheet" type="text/css" href="../styles/datapages.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../styles/bigbuttons.css" media="screen" />

    <style media="screen" type="text/css">
         #titleframe{
            margin:0 auto;
            width:630px;
            height:170px;
            float:center;
            clear:left;
            margin-bottom:50px;
        }

        .titletext{
            position:relative;
            float:left;
            padding-left:50px;
            padding-top:50px;
            width:350px;
        }

    </style>

    <script type="text/javascript" src="../js/session_handler.js"></script>

    <?php
        // verify that an active session exists before loading the page
        $path = $_SERVER['DOCUMENT_ROOT'];
        $session_verifier = $path . '/scripts/administration/session_verifier.php';
        include ($session_verifier);
        if (session_verified() == '0') 
            header( 'Location: ../signin.php' ) ;
    ?>

</head>

<body>

    <script type="text/javascript">
        
        function createAdmin() {

            //check for excessive inactivity
            if (verifySession() != 1) {
                logout();
                return false;
            }

            refreshSession();
 
            var security_code   = document.getElementById("security_code").value;
            var user_name        = document.getElementById("user_name").value;
            var plaintext1      = document.getElementById("plaintext1").value;
            var plaintext2      = document.getElementById("plaintext2").value;
                        
            var xmlhttp     = new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                   if (response == "success") {
                        alert("New Administrator Added Sucessfully");
                        window.location.href = "administration.php";
                    } else {
                        document.getElementById("rightsidebox").innerHTML=response;
                    }
                }
            }

            var content =   "security_code="    +encodeURIComponent(security_code)+
                            "&user_name="       +encodeURIComponent(user_name)+
                            "&plaintext1="      +encodeURIComponent(plaintext1)+
                            "&plaintext2="      +encodeURIComponent(plaintext2);
            
            xmlhttp.open("POST", "../scripts/administration/create_admin.php", true);
            xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(content);

            return false;
        }
    
    </script>

    <div id="titleframe">
        <div class="logoimage">
            <img id="mainpic" src="../images/logo.png">
        </div>

        <div class="titletext">
            <h1>New Administrator</h1>
            <p>Use this area to add a new database administrator.</p>
        </div>
    
    </div>
    
    <div id="userentry">

        <div id="buttonstack">
            <button type="submit" class="bigbutton" onClick="createAdmin()">Create Administrator</button> 
            <button type="submit" class="bigbutton" onClick="parent.location='administration.php'">Cancel</button> 
        </div>
        
        <div id="additem">
        
            <form id="form" name="form">
                <label>Security Code
                    <span class="small">Enter the security code provided to you</span>
                </label>
                <input type="text" id="security_code" autofocus="autofocus"/>

                <label>Username
                    <span class="small">Enter new username</span>
                </label>
                <input type="text" id="user_name" />
    
                <label>Password
                    <span class="small">Enter user's password</span>
                </label>
                <input type="password" id="plaintext1" />
    
                <label>Confirm Password
                    <span class="small">Confirm user's password</span>
                </label>
                <input type="password" id="plaintext2" />
              
            </form>

        </div>
    
    </div>
    
    <div id="rightsidebox" ></div>
</body>

</html>
